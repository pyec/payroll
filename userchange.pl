### ***********************************************************
### File: userchange.pl
### Purpose: To display the 'Select Period...' menu for viewing
###		current or old pay period hours or to display the user 
###		profile in edit mode, if that option was selected from 
###		the main menu.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$choice = $FORM{'choice'};

# Validate the login attempt
if ( validateEmpLogin($name, $pass) == 1) {
	#Login successful, display the selected page
	
	if($choice eq "Modify Profile"){
		#Display the user profile for editing
		printHeader;
		print <<EndHTML;
			<p><font color="#0000a0">Welcome $strEmpName.  Your Profile is as follows:</p>

			<FORM onSubmit="return checkPwd();" action="passmod.pl" method="post">
				<input type="hidden" name="userid" value="$strUserId">
				User Name:<input type="text" name="uname" value="$strLogin"><br>
				Name : $strEmpName<input type="hidden" name="name" value="$strEmpName"><br>
				Business Unit : $strEmpBU<input type="hidden" name="bu" value="$strEmpBU"><br>
				Division : $strEmpDiv<input type="hidden" name="division" value="$strEmpDiv"><br>
				Supervisor : $strEmpMgr<input type="hidden" name="mgr" value="$strEmpMgr"><br>
				Employee Number : $strEmpNum<input type="hidden" name="empnum" value="$strEmpNum"><br>
				Old Password : $pass<input type="hidden" name="pass" value="$pass"><br>
				New Password : <input type="password" name="newpass1" value="$pass"><br>
				Confirm New Password : <input type="password" name="newpass2" value="$pass"><br>
				<input type="SUBMIT" value="Submit"><input type="Reset" value="Reset">
			</form>

			<FORM action='user.pl' method='post' name='menu'>
				<input type='hidden' name='userid' value='$strUserId'>
				<input type='hidden' name='name' value='$name'>
				<input type='hidden' name='pass' value='$pass'>
				<input type="submit" ID="hiddenButton" value="Main Menu">	
			</FORM>
			$strLOGOUT_LINK
	</body>
</html>
EndHTML
		exit;

	}elsif($choice eq "Input Hours"){
		#Display the period select page for Inputting Hours
		
		#Get the HTML for the list of available payperiods
		$options = getPeriods("New");
		
		printHeader;
		print <<EndHTML;
			<p><font color="#0000a0">$strEmpName, please select the pay period you would like to edit?</p>
			<form action="empinput.pl" method="post">
				<input type="hidden" name="userid" value="$strUserId">
				<input type="hidden" name="pass" value="$pass">
				<input type="hidden" name="name" value="$name">
				<P>Payperiod : 
				<select name="period">
					$options
				</select></P>
				<input type="submit" value="Submit">
			</form>

			<FORM name="PayPeriodChart">
				<INPUT type='button' value='Pay Period Chart' onClick="window.open('payperiods.pl?year=$strCurrentYear&uid=$strUserId','mywindow','width=450,height=200,scrollbars=yes,resizable=yes')">
			</FORM>
			<br>
			
			<FORM action='user.pl' method='post' name='menu'>
				<input type='hidden' name='userid' value='$strUserId'>
				<input type='hidden' name='name' value='$name'>
				<input type='hidden' name='pass' value='$pass'>
				<input type="submit" ID="hiddenButton" value="Main Menu">
			</FORM>
			$strLOGOUT_LINK

	</body>
</html>
EndHTML
		exit;

	}elsif($choice eq "View Old Hours"){
		#Display the period select page for Viewing Old Hours
		
		#Get the HTML for the list of available payperiods
		$options = getPeriods("Old");
		
		printHeader;
		print <<EndHTML;
			<p><font color="#0000a0">$strEmpName, please select the pay period you would like to view?</p>
			<form action="displayhours.pl" method="post">
				<input type="hidden" name="userid" value="$strUserId">
				<input type="hidden" name="pass" value="$pass">
				<input type="hidden" name="name" value="$name">
				<P>Payperiod : 
				<select name="period">
					$options
				</select></P>
				<input type="submit" value="Submit">
			</form>

			<FORM name="PayPeriodChart">
				<INPUT type='button' value='Pay Period Chart' onClick="window.open('payperiods.pl?year=$strCurrentYear&uid=$strUserId','mywindow','width=450,height=200,scrollbars=yes,resizable=yes')">
			</FORM>
			<br>
			
			<FORM action='user.pl' method='post' name='menu'>
				<input type='hidden' name='userid' value='$strUserId'>
				<input type='hidden' name='name' value='$name'>
				<input type='hidden' name='pass' value='$pass'>
				<input type="submit" ID="hiddenButton" value="Main Menu">	
			</FORM>
			$strLOGOUT_LINK

		</body>
		</html>
EndHTML
		exit;
	}else{
		printHeader;
		print "<P>'$choice' is not a valid selection</P>";
	}
}else{
	#Display a login error
	printEmpLoginErr;
}



######################################################################
### Generate an <OPTION> list of pay periods
### INPUT:
### - Old: string indicating to retrieve the "Old" or future pay periods
### OUTPUT:
###	- strHTML: Contains the <OPTION> list of pay periods
######################################################################
sub getPeriods{
	# Get parameter indicating old or new periods
	my($strOld) = @_;
	
	# Connect to the local db
	db_connect;
	
	# If no supervisor, user must be own supervisor
	if ($strEmpMgrId eq "") {
		$strEmpMgrId = $strUserId;
	}
	### Retrieve the current pay period for the current user
	$strSQL = "SELECT CurrentPeriodId
				FROM Supervisors
				WHERE SupervisorId = '$strEmpMgrId'";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	($strCurrentPeriodNum) = $sql_statement->fetchrow_array;

	# Get either the old or current periods, depending on the parameter
	if ($strOld eq "Old") {
		### Retrieve the last $nOLD_PERIODS_TO_DISPLAY Pay Periods
		$strSQL =  "SELECT PeriodId, Year 
					FROM Periods
					WHERE PeriodId < '$strCurrentPeriodNum'
					ORDER BY PeriodId DESC";
		$nMaxPeriods = $nOLD_PERIODS_TO_DISPLAY;
	}else{
		### Retrieve the next $nPERIODS_TO_DISPLAY Pay Periods
		$strSQL =  "SELECT PeriodId, Year 
					FROM Periods
					WHERE PeriodId >= '$strCurrentPeriodNum'
					ORDER BY PeriodId";
		$nMaxPeriods = $nPERIODS_TO_DISPLAY;
	}
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	my($strHTML) = "";
	my($nCountOfPeriods) = 0;
	while (my($strPeriodId, $strYear) = $sql_statement->fetchrow_array and $nCountOfPeriods++ < $nMaxPeriods) {
		# Add an <OPTION> for each payperiod
		$strHTML .= "<OPTION value='$strPeriodId'>$strPeriodId</OPTION>\n";

		# Save the current year
		if ($nCountOfPeriods == 1) {
			$strCurrentYear = $strYear;
		}
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;
	
	# return the HTML to display the Select list
	return $strHTML;
}
	

exit;