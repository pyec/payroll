### ***********************************************************
### File: moduser.pl
### Purpose: To display a picklist of users to delete, modify
###		or accept entry of a new user
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;

#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');


# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n", $template_header->output;


# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$mgr = $FORM{'mgr'};
$strLOGIN_PAGE = "/CorporateProjects/Timesheets/payadmin.html";
$strADM_LOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";

#Initialize the arrays to store the user data for updates (to be
#	used in client-side javascript)
@aUserId = ();
@aLogin = ();
@aEmployeeName = ();
@aEmployeeNumber = ();
@aStatus = ();
@aOldPwd = ();
#Variable containing the number of arrays above
$nElementCount = 6;

#Default the mgr to the current user if mgr is not specified
if ($mgr eq "") {
	$mgr = $strUserId;
}

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, display the user information

	#Build the option list of User Statuses from the db
	$strStatusList = getStatus();
	
	### Get the subordinates of the specified mgr
	# Connect to the local db
	db_connect;

	# Display the page
#	printHeader;
	print <<EndHTML;

	<P><font color="#0000A0">$strEmpName, please perform one of the following User functions:</font></P>
	
	<form action="moduser_submittemp.pl" method="post" name="main">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="hidden" name="mgr" value="$mgr">
		<input type="hidden" name="fn_delete" value="">
		<input type="hidden" name="fn_update" value="">
		<input type="hidden" name="fn_add" value="">

	<TABLE border='2' align='center' cellpadding='5'>
		<TR>
			<TD bgcolor="#EECC66"><strong>Delete User:</strong></TD>
			<TD bgcolor="#EECC66"><strong>Modify User:</strong></TD>
			<TD bgcolor="#EECC66"><strong>Add New User:</strong></TD>
		</TR>
		<TR>
			<TD valign='top'><P>Users:<BR>
				<SELECT name='delete_user'>
EndHTML

	### Get the users for the specified manager
	$strSQL = "SELECT UserId, Login, EmployeeName, EmployeeNumber, StatusId, Password  
				FROM qrySel_UserProfile 
				WHERE SupervisorId = '$mgr' 
				ORDER BY EmployeeName";
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

	$strUserList = "";
	$nUserCount = 0;
	while (($strCurrentUser, $strCurrentLogin, $strCurrentEmpName, 
			$strCurrentEmpNum, $strCurrentStatus, $strCurrentPwd) = $sql_statement->fetchrow_array) {
		$strUserList .= "					<OPTION value='$strCurrentUser'>$strCurrentEmpName  ($strCurrentEmpNum)</OPTION>\n";

		#Store updatable info in an array to be used in JS on the client-side
		$aUserId[$nUserCount] = $strCurrentUser;
		$aLogin[$nUserCount] = $strCurrentLogin;
		$aEmployeeName[$nUserCount] = $strCurrentEmpName;
		$aEmployeeNumber[$nUserCount] = $strCurrentEmpNum;
		$aStatus[$nUserCount] = $strCurrentStatus;
		$aOldPwd[$nUserCount] = $strCurrentPwd;
		
		#Increment the user count
		$nUserCount++;
	}
	print ($strUserList);
	print <<EndHTML;
				</SELECT><BR></P>
				<P align='center'><INPUT type='button' value='Delete' name='cmdDelete' onClick='checkForm(this.value);'></P>
			</TD>

			<TD valign='top'><P>Users:<BR>
				<SELECT name='upd_user' onChange='setUser(this.value);'>
					<OPTION value=''>Select To Update</OPTION>
				$strUserList
				</SELECT></P>
				<TABLE>
					<TR>
						<TD><font color="#FF0000">*</font>Login:</TD><TD><INPUT maxlength='8' size='10' name='upd_login' onChange='this.value=this.value.toLowerCase()'></TD>
					</TR>
					<TR>
						<TD><font color="#FF0000">*</font>Employee Name:</TD><TD><INPUT maxlength='40' size='15' name='upd_emp_name'></TD>
					</TR>
					<TR>
						<TD><font color="#FF0000">*</font>Employee Number:</TD><TD><INPUT maxlength='40' size='15' name='upd_emp_num'></TD>
					</TR>
					<TR>
						<TD>Status:</TD>
						<TD>
							<SELECT name='upd_status'>
								$strStatusList
							</SELECT>
						</TD>
					</TR>
					<TR>
						<TD>Old Password:</TD><TD><INPUT maxlength='40' size='15' name='upd_old_pwd' onFocus='this.blur()'></TD>
					</TR>
					<TR>
						<TD>Password:</TD><TD><INPUT type='password' maxlength='40' size='15' name='upd_pwd'></TD>
					</TR>
					<TR>
						<TD>Confirm Password:</TD><TD><INPUT type='password' maxlength='40' size='15' name='upd_conf_pwd'></TD>
					</TR>
					<TR>
						<TD colspan='2' align='center'><INPUT type='button' value='Update' name='cmdUpdate' onClick='checkForm(this.value);'></TD>
					</TR>
				</TABLE>
			</TD>

			<TD valign='top'>				<TABLE>
					<TR>
						<TD><font color="#FF0000">*</font>Login:</TD><TD><INPUT maxlength='8' size='10' name='add_login' onChange='this.value=this.value.toLowerCase()'></TD>
					</TR>
					<TR>
						<TD><font color="#FF0000">*</font>Employee Name:</TD><TD><INPUT maxlength='40' size='15' name='add_emp_name'></TD>
					</TR>
					<TR>
						<TD><font color="#FF0000">*</font>Employee Number:</TD><TD><INPUT maxlength='40' size='15' name='add_emp_num'></TD>
					</TR>
					<TR>
						<TD>Status:</TD>
						<TD>
							<SELECT name='add_status'>
								$strStatusList
							</SELECT>
						</TD>
					</TR>
					<TR>
						<TD><font color="#FF0000">*</font>Password:</TD><TD><INPUT type='password' maxlength='40' size='15' name='add_pwd'></TD>
					</TR>
					<TR>
						<TD><font color="#FF0000">*</font>Confirm Password:</TD><TD><INPUT type='password' maxlength='40' size='15' name='add_conf_pwd'></TD>
					</TR>
					<TR>
						<TD colspan='2' align='center'><INPUT type='button' value='Add' name='cmdAdd' onClick='checkForm(this.value);'></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>

	</form>

	<P align='center'><form action="adminlogintemp.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="submit" value="Main Menu">
	</form></P>

	$strADM_LOGOUT_LINK

<SCRIPT language="JavaScript" type="text/javascript">
// Array of user info
aUsers = new Array($nUserCount);

EndHTML

	# Generate the Client-side JavaScript that will display the user
	#	info for updating
	for ($nCurrentUser = 0; $nCurrentUser < $nUserCount; $nCurrentUser++) {
		print ("	aUsers[$nCurrentUser] = new Array ($nElementCount);\n");
		print ("	aUsers[$nCurrentUser][0] = '$aUserId[$nCurrentUser]';\n");
		print ("	aUsers[$nCurrentUser][1] = '$aLogin[$nCurrentUser]';\n");
		print ("	aUsers[$nCurrentUser][2] = '$aEmployeeName[$nCurrentUser]';\n");
		print ("	aUsers[$nCurrentUser][3] = '$aEmployeeNumber[$nCurrentUser]';\n");
		print ("	aUsers[$nCurrentUser][4] = '$aStatus[$nCurrentUser]';\n");
		print ("	aUsers[$nCurrentUser][5] = '$aOldPwd[$nCurrentUser]';\n");
	}
	print <<EndHTML;

// function to display  the user info in the update text boxes
//	when selected from the dropdown
function setUser(strUserId) {

	//Find the index of the user selected
	for (nIndex=0; nIndex < aUsers.length; nIndex++) {
		if (aUsers[nIndex][0] == strUserId) {
			// Found the user, use this index value;
			break;
		}
	}
	
	//Using the index, display the user info in the text boxes
	if (nIndex < aUsers.length) {
		document.main.upd_user.value = aUsers[nIndex][0];
		document.main.upd_login.value = aUsers[nIndex][1];
		document.main.upd_pwd.value = '';
		document.main.upd_emp_name.value = aUsers[nIndex][2];
		document.main.upd_emp_num.value = aUsers[nIndex][3];
		document.main.upd_old_pwd.value = aUsers[nIndex][5];
		
		oSelectStatus = document.main.upd_status;
		for (nCount=0; nCount < oSelectStatus.options.length; nCount++) {
			if (oSelectStatus.options[nCount].value==aUsers[nIndex][4]) {
				oSelectStatus.selectedIndex=nCount;
			}
		}

	}else{
		// User not found, clear the update boxes
		document.main.upd_user.value = '';
		document.main.upd_login.value = '';
		document.main.upd_old_pwd.value = '';
		document.main.upd_pwd.value = '';
		document.main.upd_conf_pwd.value = '';
		document.main.upd_emp_name.value = '';
		document.main.upd_emp_num.value = '';
		document.main.upd_status.selectedIndex = '-1';
	}

}

// Function to validate the form input before submitting
function checkForm(strFunction) {

	//Init error flag
	bContinue = true;
	
	if (strFunction == 'Delete') {
		//Delete - confirm if 'delete' is selected
		if (confirm("Are you sure you wish to delete user: " + 
				document.main.delete_user.value + "?")) {
			document.main.fn_delete.value = strFunction;
			document.main.submit();
		}

	}else{
		if (strFunction == 'Update') {
			//Update - validate required fields and password
			strUser = document.main.upd_user.value;
			strLogin = document.main.upd_login.value;
			strName = document.main.upd_emp_name.value;
			strEmpNum = document.main.upd_emp_num.value;
			strPwd = document.main.upd_pwd.value;
			strConf_Pwd = document.main.upd_conf_pwd.value;

			//Validate that the dropdown was selected
			if (strUser == '') {
				alert("You must select from the dropdown to edit a user.");
				document.main.upd_user.focus();
				bContinue = false;
			}

			//Validate Login is supplied
			if (bContinue) {
				if (strLogin == '') {
					alert("Login cannot be blank. Please re-enter.");
					document.main.upd_login.focus();
					bContinue = false;
				}
			}

			//Validate Name is supplied
			if (bContinue) {
				if (strName == '') {
					alert("Employee Name cannot be blank. Please re-enter.");
					document.main.upd_emp_name.focus();
					bContinue = false;
				}
			}

			if (bContinue) {
				//Validate EmpNum is numeric and supplied
				nEmpNum = parseInt(strEmpNum);
				if (isNaN(nEmpNum) && strEmpNum != null && strEmpNum != "") {
					alert("Employee Number must be numeric. Please re-enter.");
					document.main.upd_emp_num.focus();
					document.main.upd_emp_num.select();
					bContinue = false;
				}else{
					// Use the parseInt value for robustness
					if (strEmpNum != null && strEmpNum != "") {
						document.main.upd_emp_num.value = nEmpNum;
					}else{
						alert("Employee Number cannot be blank. Please re-enter.");
						document.main.upd_emp_num.focus();
						bContinue = false;
					}
				}
			}

			if (bContinue) {
				//Validate passwords
				if (strPwd != '' && strPwd != strConf_Pwd) {
					alert("Passwords are not the same. Please re-enter them.");
					document.main.upd_pwd.value='';
					document.main.upd_conf_pwd.value='';
					document.main.upd_pwd.focus();
					bContinue = false;
				}
			}

			if (bContinue) {
				//All validations passed - submit the form
				document.main.fn_update.value = strFunction;
				document.main.submit();
			}

		}else{
			if (strFunction == 'Add') {
				//Add - validate required fields and password
				strLogin = document.main.add_login.value;
				strName = document.main.add_emp_name.value;
				strEmpNum = document.main.add_emp_num.value;
				strPwd = document.main.add_pwd.value;
				strConf_Pwd = document.main.add_conf_pwd.value;
	
				//Validate Login is supplied
				if (strLogin == '') {
					alert("Login cannot be blank. Please re-enter.");
					document.main.add_login.focus();
					bContinue = false;
				}
	
				//Validate Name is supplied
				if (bContinue) {
					if (strName == '') {
						alert("Employee Name cannot be blank. Please re-enter.");
						document.main.add_emp_name.focus();
						bContinue = false;
					}
				}
	
				if (bContinue) {
					//Validate EmpNum is numeric if supplied
					nEmpNum = parseInt(strEmpNum);
					if (isNaN(nEmpNum) && strEmpNum != null && strEmpNum != "") {
						alert("Employee Number must be numeric. Please re-enter.");
						document.main.add_emp_num.focus();
						document.main.add_emp_num.select();
						bContinue = false;
					}else{
						// Use the parseInt value for robustness
						if (strEmpNum != null && strEmpNum != "") {
							document.main.add_emp_num.value = nEmpNum;
						}else{
							alert("Employee Number cannot be blank. Please re-enter.");
							document.main.add_emp_num.focus();
							bContinue = false;
						}
					}
				}
	
				if (bContinue) {
					//Validate passwords
					if (strPwd == '' || strPwd != strConf_Pwd) {
						alert("Passwords are not the same or are blank. Please re-enter them.");
						document.main.add_pwd.value='';
						document.main.add_conf_pwd.value='';
						document.main.add_pwd.focus();
						bContinue = false;
					}
				}

				if (bContinue) {
					//All validations passed - submit the form
					document.main.fn_add.value = strFunction;
					document.main.submit();
				}
			}
		}
	}
}
</SCRIPT>
</BODY>
</HTML>
EndHTML
	
	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

print $template_footer->output;
exit;

}else{
	#Display a login error
	printAdmLoginErr;
}
print $template_footer->output;
exit;

######################################################################
### Generate an <OPTION> list of user statuses
### OUTPUT:
###	- $strChoices: string containing <OPTION> list of user statuses
######################################################################
sub getStatus {
	# Connect to the local db
	db_connect;
	
	# Retrieve the User Status information
	$strSQL = "SELECT StatusId, StatusDesc
				FROM UserStatus
				ORDER BY StatusId";
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

	$strChoices = "";	
	while (($strStatusId, $strStatusDesc) = $sql_statement->fetchrow_array) {
		$strChoices .= "<OPTION value='$strStatusId'>$strStatusDesc</OPTION>\n";
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;
	
	return $strChoices;
}
