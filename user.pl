### ***********************************************************
### File: user.pl
### Purpose: to validate the login of an employee into the
### 		timesheet application.  See adminlogin.pl for
###			login of administrative users.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$name = $FORM{'name'};
$pass = $FORM{'pass'};

# Validate the login attempt
if ( validateEmpLogin($name, $pass) == 1) {
	#Login successful, display the menu page
	printHeader;
	print <<EndHTML;

	<p><font color="#0000a0">$strEmpName, what would you like to do?</p>
	
	<form action="userchange.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<select name="choice">
			<option value="Input Hours">Input Hours</option>
			<option value="Modify Profile">Modify Profile</option>
			<option value="View Old Hours">View Old Hours</option>
		</select>
		<input type="submit" value="Submit">
	</form>
	<br>
	
	$strLOGOUT_LINK
	
</body>
</html>

EndHTML

}else{
	#Display a login error
	printEmpLoginErr;
}

exit;