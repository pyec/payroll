### ***********************************************************
### File: admin_sel_div.pl
### Purpose: To display a picklist of division/supervisors for the 
###		specified Business Unit, and then submit that to the 
###		appropriate page depending on the selected admin request
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$busunit = $FORM{'bu'};
$adminchoice = $FORM{'adminchoice'};

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, display the BU selection list and redirect accordingly
	
	# Connect to the local db
	db_connect;
	
	#If adminchoice is 'print' only show supervisors where PrintAllFlag is true
	if ($adminchoice eq 'print') {
		$strTarget = "qrySel_PrintAllSupervisors";
	}else{
		$strTarget = "qrySel_AllSupervisors";
	}
	### Retrieve the Business Unit information
	$strSQL = "SELECT SupervisorId, EmployeeName, DivisionDesc   
				FROM $strTarget
				WHERE BUId = '$busunit'
				ORDER BY DivisionDesc, EmployeeName";
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

	$strChoices = "";	
	while (($strSupervisorId, $strSupervisorName, $strDivDesc) = $sql_statement->fetchrow_array) {
		$strChoices .= "<OPTION value='$strSupervisorId'>$strSupervisorName ($strDivDesc)</OPTION>\n";
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	#Define the target based on the selected admin request
	if($adminchoice eq "display"){
		### Display Hours is selected
		$strTarget = "adminview.pl";
	}elsif($adminchoice eq "print"){
		### Print Hours is selected
		$strTarget = "adminview.pl";
	}elsif($adminchoice eq "advance"){
		### Change Pay Period is selected
		$strTarget = "adminchange.pl";
	}elsif($adminchoice eq "users"){
		### Modify Users is selected
		$strTarget = "moduser.pl";
	}elsif($adminchoice eq "codes"){
		### Modify Pay Codes is selected
		$strTarget = "codechange.pl";
	}elsif($adminchoice eq "display_old"){
		### Display Old Pay Period is selected
		$strTarget = "admin_sel_per.pl";
	}else{
		#Unknown choice, return to the menu screen
		$strTarget = "adminlogin.pl";
	}
	
	printHeader;
	print <<EndHTML;
	<font color="#0000a0"><h1 align=center>HRM Payroll Administration</h1></font>
	<p><font color="#0000a0">$strEmpName, please select the Supervisor/Division to work with?</p>
	
	<form action="$strTarget" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="hidden" name="adminchoice" value="$adminchoice">
		<P><select name="mgr">
			$strChoices
		</select></P>
		<input type="submit" value="Submit">
	</form>
	<br>
	
	<form action="adminlogin.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="submit" ID="hiddenButton" value="Main Menu">	
	</form>

	$strADM_LOGOUT_LINK
	
</body>
</html>

EndHTML

	exit;

}else{
	#Display a login error
	printAdmLoginErr;
}

exit;
