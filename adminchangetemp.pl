### ***********************************************************
### File: adminchange.pl
### Purpose: Display the screen to advance the pay period
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;

#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');


# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n", $template_header->output;

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$mgr = $FORM{'mgr'};
$adminchoice = $FORM{'adminchoice'};
$strLOGIN_PAGE = "/CorporateProjects/Timesheets/payadmin.html";
$strADM_LOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, display the current period and option to change it

	# Connect to the local db
	db_connect;
	
	### Retrieve the CURRENT pay period for the specified supervisor
	$strSQL = "SELECT CurrentPeriodId, EmployeeName
				FROM qrySel_AllSupervisors
				WHERE SupervisorId = '$mgr'";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	($strCurrentPeriodNum, $strMgrName) = $sql_statement->fetchrow_array;

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	# Get period start, end and pay dates in string form
	getPeriodDates($strCurrentPeriodNum);

	#Define instruction string
	if ('$strUserId' eq '$mgr') {
		$strInstructions = "$strEmpName, please confirm the changing of the pay period:";
	}else{
		$strInstructions = "$strEmpName, please confirm the changing of $strMgrName\'s pay period:";
	}
	
#	printHeader;
	print <<EndHTML;
		<font color="#0000a0"><h1 align=center>HRM Payroll Administration</h1></font>
		<p><font color="#0000a0">$strInstructions</p>

		<form action="adminchange_submittemp.pl" method="post" name="ChangePeriod" id="ChangePeriod">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
			<input type="hidden" name="mgr" value="$mgr">
	
		<TABLE cellpadding="2" border="1">
			<TR>
				<TD bgcolor='#eecc66' align=middle><STRONG>Current Period</STRONG></TD>
				<TD bgcolor='#eecc66' align=middle><STRONG>Next Period</STRONG></TD>
				<TD bgcolor='#eecc66' align=middle><STRONG>Previous Period<BR></STRONG>[Only for corrections:]</TD>
			</TR>
			<TR>
				<TD>
					PeriodID: $strCurrentPeriodNum<BR>
					Start Date: $strStartDate<BR>
					End Date: $strEndDate<BR>
					Pay Day: $strPayDate
				</TD>
EndHTML

	### Retrieve the NEXT pay period for the specified supervisor
	# Connect to the local db
	db_connect;
	
	$strSQL = "SELECT PeriodId
				FROM Periods
				WHERE PeriodId > '$strCurrentPeriodNum' 
				ORDER BY PeriodId";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	($strNextPeriodNum) = $sql_statement->fetchrow_array;

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	# Get period start, end and pay dates in string form
	getPeriodDates($strNextPeriodNum);

	print <<EndHTML;
				<TD>
					<strong>PeriodID: $strNextPeriodNum<BR>
					Start Date: $strStartDate<BR>
					End Date: $strEndDate<BR>
					Pay Day: $strPayDate</strong>
				</TD>
EndHTML

	### Retrieve the PREVIOUS pay period for the specified supervisor
	# Connect to the local db
	db_connect;
	
	$strSQL = "SELECT PeriodId
				FROM Periods
				WHERE PeriodId < '$strCurrentPeriodNum' 
				ORDER BY PeriodId DESC";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	($strPrevPeriodNum) = $sql_statement->fetchrow_array;

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	# Get period start, end and pay dates in string form
	getPeriodDates($strPrevPeriodNum);

	print <<EndHTML;
				<TD><font color="#c0c0c0">
					PeriodID: $strPrevPeriodNum<BR>
					Start Date: $strStartDate<BR>
					End Date: $strEndDate<BR>
					Pay Day: $strPayDate</font>
				</TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD align-center><INPUT type="button" style="FONT-WEIGHT: bold" value="*** Next Pay Period ***" onClick="setPeriod('$strNextPeriodNum');"></TD>
				<TD align-center><font color="#c0c0c0">Only for corrections:<BR><input type="button" value="[Previous Pay Period]" onClick="setPeriod('$strPrevPeriodNum');"></font></TD>
			</TR>
		</TABLE>

			<input type="hidden" name="new_period" value="$strNextPeriodNum">
		</FORM>
		
		<form action="adminlogintemp.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
			<input type="submit" ID="hiddenButton" value="Main Menu">	
		</form>

		$strADM_LOGOUT_LINK

	</BODY>
	
	<script language="JavaScript" type="text/javascript">
		function setPeriod(strPeriod) {
			if (strPeriod > '$strCurrentPeriodNum') {
				if (confirm("Employees will no longer be able to enter data for the current period.  Are you certain?")) {
					document.ChangePeriod.elements['new_period'].value = strPeriod;
					document.ChangePeriod.submit();
				}
			}else{
				if (confirm("This is to revert to the previous pay period.  You should only be doing this if you mistakenly advanced the pay period previously.  Are you certain?")) {
					document.ChangePeriod.elements['new_period'].value = strPeriod;
					document.ChangePeriod.submit();
				}
			}
		}
	</script>
</HTML>
EndHTML

}else{
	#Display a login error
	printAdmLoginErr;
}

print $template_footer->output;

exit;