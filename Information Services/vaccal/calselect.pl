#module for setting the current working directory
use hrm_homedir;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$value =~ s/\n/ /g;	# replace newlines with spaces
	$value =~ s/\r//g;	# remove hard returns
	$value =~ s/\cM//g;	# delete ^M's
	$FORM{$name} = $value;
}



getheader();
print <<EndHTML;<html><head><title>IS Vacation Calendar</title>$header<br><script language="JavaScript">var isIE4 = ((navigator.appName == "Microsoft Internet Explorer") && (parseInt(navigator.appVersion) >= 4 ))
var isIE3 = (navigator.appName == "") // kind of dumb, but it works
var docOut = self  // output on same document (for IE4 and IE3)

function CMonth(sName, iMaxDays) {
        this.sName=sName
        this.iMaxDays=iMaxDays
}

/////////////////////////////////////////////

tblCMonth=new Array(12)
tblCMonth[0]=new CMonth("January", 31)
tblCMonth[1]=new CMonth("February", 28)
tblCMonth[2]=new CMonth("March", 31)
tblCMonth[3]=new CMonth("April", 30)
tblCMonth[4]=new CMonth("May", 31)
tblCMonth[5]=new CMonth("June", 30)
tblCMonth[6]=new CMonth("July", 31)
tblCMonth[7]=new CMonth("August", 31)
tblCMonth[8]=new CMonth("September", 30)
tblCMonth[9]=new CMonth("October", 31)
tblCMonth[10]=new CMonth("November", 30)
tblCMonth[11]=new CMonth("December", 31)

/////////////////////////////////////////////

// Netscape 3.x, 4.x workaround
function OpenWindow()   {
     NewWindow=window.open("")
     return NewWindow
}

/////////////////////////////////////////////

function isLeap(year) {
        if ((year % 100 != 0) && (year % 4 == 0) || (year % 400 == 0)) {
                return true
        } 
        return false
}

/////////////////////////////////////////////

function julianDay(iDay, iMonth, iYear) {

        var j = iDay

        for(var i = 1; i < iMonth; i++) {
                j += tblCMonth[i-1].iMaxDays
        }
        return j
}

/////////////////////////////////////////////

// return the day of week: 1=Sun, 2=Mon, ..., 7=Sat
function dow(year, julian) {

        // note this grotesque hack of ... - (-julian) + ....!
        // apparently, JS thinks 2+3 = 23 (concatenation instead of addition)
        
        var code = (year - (-julian) + Math.floor( (year-1)/4 ) 
                  - Math.floor( (year-1)/100 ) + Math.floor( (year-1)/400 )) % 7
        if (code == 0) {
                return 7
        }
        return code
}

/////////////////////////////////////////////

function generateCalendar() {
        
        if (isIE4 == false && isIE3 == false) {
                docOut = OpenWindow() // open a new window for Netscape users
        }
        var iYear = document.forms[0].txYear.value
        var iMonth=(document.forms[0].selectMonth.selectedIndex) + 1
        var iShowWholeYear = (document.forms[0].bWholeYear.checked == true ? 1 : 0)
		var vacdays=document.forms[0].daysonvac.value
		var su=document.forms[0].dr.value
        if (iShowWholeYear == 0) {
                showCalendar(iYear, iMonth, true,vacdays, su )
        } else {
                showYearCal(iYear)
        }
        
}



// show calendar for a particular year and month
function showCalendar(iYear, iMonth, bOnlyMonth, vacdays,su) {
        
        var sResult = ""
        var chSpace = "&nbsp;"
        var bSunTrFlag = 0
        var savYear = iYear
		var sa=su

        if (iYear > 1582 && iYear <= 25000) {
                
                // adjust for February
                if (isLeap(iYear) == true) {
                        tblCMonth[1].iMaxDays=29
                } else {
                        tblCMonth[1].iMaxDays=28
                }
                
                if (bOnlyMonth == true) {

                        // begin Dynamic HTML Generation
                        docOut.document.writeln("<HTML><HEAD><TITLE>IS Vacation Calendar</TITLE></HEAD><BODY BGCOLOR=white>")
                        docOut.document.writeln("<DIV ALIGN=CENTER><H1>Calendar for: "+tblCMonth[iMonth-1].sName+" "+ savYear +"</H1><HR>")
                	
				} else {
                        docOut.document.writeln("<DIV ALIGN=CENTER><BIG>"+tblCMonth[iMonth-1].sName+"</BIG><BR>")
                }

                
                // calendar is output as a table - these are the table params
                docOut.document.writeln("<TABLE COLS=7 BGCOLOR=white BORDER=0>")
                // this is the header dislay of days of week            
                docOut.document.writeln("<THEAD><TR><TH ALIGN=right><FONT FACE=courier COLOR=red>Sun</FONT></TH>")
                docOut.document.writeln("<TH ALIGN=right><FONT FACE=courier>Mon</FONT></TH><TH ALIGN=right><FONT FACE=courier>Tue</FONT></TH>")
                docOut.document.writeln("<TH ALIGN=right><FONT FACE=courier>Wed</FONT></TH><TH ALIGN=right><FONT FACE=courier>Thu</FONT></TH>")
                docOut.document.writeln("<TH ALIGN=right><FONT FACE=courier>Fri</FONT></TH><TH ALIGN=right><FONT FACE=courier COLOR=red>Sat</FONT></TH></TR>")
                docOut.document.writeln("<TBODY><TR>")
                
                var jday = julianDay(1, iMonth, iYear)
                var fday = dow(iYear, jday)
                
                // write the 1st of the month
                for (var idx = 1; idx < fday; idx++) {
                        docOut.document.writeln("<TD>&nbsp;&nbsp;</TD>")
                }
                if (fday > 1&& fday<6)
                        docOut.document.writeln("<TD ALIGN=right><FONT FACE=courier>&nbsp;<a href='/cgi-bin/payroll/Information%20Services/vaccal/cal.pl?1|"+tblCMonth[iMonth-1].sName+"|"+savYear+"|"+vacdays+"|"+sa+"'>1</a></FONT></TD>")
                else
                        docOut.document.writeln("<TD ALIGN=right><FONT FACE=courier COLOR=red>&nbsp;<a href='/cgi-bin/payroll/Information%20Services/vaccal/cal.pl?1|"+tblCMonth[iMonth-1].sName+"|"+savYear+"|"+vacdays+"|"+sa+"'>1</a></FONT></TD>")


                for (var j = 2; j <= tblCMonth[iMonth-1].iMaxDays; j++)   {

                        if (dow(iYear, julianDay(j-1, iMonth, iYear)) == 7) {
                                docOut.document.writeln("</TR><TR>")
                                bSunTrFlag=1
                        }
						if (dow(iYear, julianDay(j-1, iMonth, iYear)) == 6) {
                                bSunTrFlag=1
                        }
                        if (j < 10)
                                chSpace = "&nbsp;"
                        else
                                chSpace = ""

                        if (bSunTrFlag == 1) {
                                docOut.document.writeln("<TD ALIGN=right><FONT FACE=courier COLOR=red>"+ chSpace +"<a href='/cgi-bin/payroll/Information%20Services/vaccal/cal.pl?"+j+"|"+tblCMonth[iMonth-1].sName+"|"+savYear+"|"+vacdays+"|"+sa+"'>"+ j+"</a>" + "</FONT></TD>")
                                bSunTrFlag = 0
                        } else {
                                docOut.document.writeln("<TD ALIGN=right><FONT FACE=courier>"+ chSpace + "<a href='/cgi-bin/payroll/Information%20Services/vaccal/cal.pl?"+j+"|"+tblCMonth[iMonth-1].sName+"|"+savYear+"|"+vacdays+"|"+sa+"'>"+j+"</a>" + "</FONT></TD>")
                        
                        }

                        
                }
                
                if (bOnlyMonth == true) {
      
                        docOut.document.writeln("</TR></TABLE></FONT>")
                         if (isIE4==true || isIE3==true) {
                                docOut.document.writeln("<P>Click <INPUT TYPE='button' VALUE='Back' onClick='history.go(-1)'>")
                                docOut.document.writeln(" or press the <EM>Back Button</EM> on your browser to go back.<P></DIV>")
								
                        }
                        docOut.document.writeln("</BODY></HTML>")
                        docOut.document.close()
                } else {
                        docOut.document.writeln("</TR></TABLE></FONT>")
                }
                
        } else { // year out of range 1583..25000
                docOut.document.writeln("Cannot display a calendar for the year ")
				docOut.document.writeln(iYear)
				docOut.document.writeln("Please enter a year between 1583 AD and 25000 AD.")
                if (isIE4==true || isIE3==true) {
                        docOut.document.writeln("<P>Click <INPUT TYPE='button' VALUE='Back' onClick='history.go(-1)'>")
                        docOut.document.writeln(" to go back.")
                }
				
                docOut.document.close()
        }
}
                        

</SCRIPT>
<FORM><br>&nbsp;&nbsp;&nbsp;<STRONG>Calendar for&nbsp;&nbsp;
<script language="JavaScript">
powi = new Date
var y=powi.getMonth()
monthn=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
document.write("<SELECT NAME='selectMonth' SIZE='1'>")
for( var t=0;t<12;t++){
document.write("<OPTION VALUE='"+monthn[t]+"'")
if(t==y){document.write("selected")}
document.write(">"+monthn[t])
}
document.write("</select>")
zowi = new Date
var z=zowi.getYear()
var browser="Netscape"
if(navigator.appName==browser){var z=zowi.getYear()+1900}
document.write("<SELECT NAME='txYear' SIZE='1'>")
for( var u=0;u<5;u++){
document.write("<OPTION VALUE='"+z+"'")
if(u==0){document.write("selected")}
document.write(">"+z)
z++
}
document.write("</select>")
</script><br>
&nbsp;&nbsp;&nbsp;<strong>Number of Days on Vacation :</strong><input type="text" name="daysonvac" size="3">
&nbsp;&nbsp;&nbsp;<INPUT TYPE="text" NAME="dr" value="Matthew MacMillan"><INPUT TYPE="hidden" NAME="bWholeYear" value="false"><INPUT TYPE="hidden" NAME="bShow3by4" value="true">
<BR><BR>&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" NAME="show" VALUE="Submit" onClick="generateCalendar()"><BR></FORM>
$footer</body></html>
EndHTML
sub dienice {
	my($msg) = @_;
	print "<h2>Error</h2>\n";
	print $msg;
	exit;
}
sub getheader{
open(INF,"header.dat") or dienice("Couldn't open: $!");					#opens the admin log file
@fat=<INF>;
close(INF);										#closes file
$header="";										#creates individual variables for array info
foreach $i(@fat){
	chomp($i);
	($head)=split(/\|/, $i);
$header = "$head";								
}	
open(INF,"foot.dat") or dienice("Couldn't open: $!");					#opens the admin log file
@nat=<INF>;
close(INF);										#closes file
	
foreach $i(@nat){
	chomp($i);
	($f)=split(/\|/, $i);
$footer = "$f";								
}	
}											#these lines print the error message to the user via a browser
exit;
