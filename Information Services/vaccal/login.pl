#module for setting the current working directory
use hrm_homedir;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$value =~ s/\n/ /g;	# replace newlines with spaces
	$value =~ s/\r//g;	# remove hard returns
	$value =~ s/\cM//g;	# delete ^M's
	$FORM{$name} = $value;
}

getheader();

$setname=$FORM{name};
$setpass=$FORM{pass};

open(INF,"../usernames.dat") or dienice("Couldn't open: $!");					
@bat=<INF>;
close(INF);		
								
foreach $i(@bat){
	
	chomp($i);
	($bka, $bra)=split(/\|/, $i);
	
	if($FORM{name} eq $bka){

		$setname=$bra;

	}							
}


open(INF,"../emplog.dat") or dienice("Couldn't open: $!");					
@lat=<INF>;
close(INF);		
								
foreach $i(@lat){

	chomp($i);
	($ka, $ra)=split(/\|/, $i);
	
	if($setpass eq $ra && $setname eq $ka){
	
		$setter=12293;

	}							
}

	
if($setter==12293){



print <<EndHTML;
<html><head><title>IS Vacation Input</title>$header<br><br>&nbsp;<p>
 <form action="/cgi-bin/payroll/Information%20Services/vaccal/vacselect.pl" method="post"><strong>What would you like to do?&nbsp;&nbsp;</strong><select name="selection"><option Value="1">View Vacation Calendar
 <option value="2">Edit Vacation Time
 <option value="3">Input Vacation Time
 </select><p><input type="hidden" name="name" value="$setname"><input type="hidden" name="pass" value="$setpass">
 <input type="submit" value="Submit">
</form>
<br><br><a href="/mainpage/businessunits/informationservices/">Return to IS Home Page</a><br><br><br>$footer</body></html>
EndHTML
exit;


}else{
print <<EndHTML;
<html><head><title>IS Vacation Input</title>$header<br><br>&nbsp;<p>
 
Sorry Wrong user name or password.<p>

<br><br><a href="/mainpage/businessunits/informationservices/vacpayemplogin.shtml">Return to Login Page</a><br><a href="/mainpage/businessunits/informationservices/">Return to IS Home Page</a><br><br><br><br><br><br>$footer</body></html>
EndHTML
exit;
}


sub getheader{
open(INF,"header.dat") or dienice("Couldn't open: $!");					#opens the admin log file
$header=<INF>;
close(INF);										#closes file
									

open(INF,"foot.dat") or dienice("Couldn't open: $!");					#opens the admin log file
$footer =<INF>;
close(INF);										#closes file

}											
exit;