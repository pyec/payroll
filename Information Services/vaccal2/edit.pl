#module for setting the current working directory
use hrm_homedir;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$value =~ s/\n/ /g;	# replace newlines with spaces
	$value =~ s/\r//g;	# remove hard returns
	$value =~ s/\cM//g;	# delete ^M's
	$FORM{$name} = $value;
}	
$referer=$ENV{'QUERY_STRING'};
$referer=~ s/%20/ /gi; 

@pass=split(/\|/, $referer);
			
$name=$pass[0];
$pass=$pass[1];

open(INF,"../emplog.dat") or dienice("Couldn't open: $!");					
@lat=<INF>;
close(INF);										
foreach $i(@lat){
	chomp($i);
	($ka, $ra)=split(/\|/, $i);
if($pass eq $ra && $name eq $ka){
$setter=12293;
}							
}	
if($setter==12293){

open(INF,"input/data.dat") or dienice("Couldn't open: $!");					
@fat=<INF>;
close(INF);	
$count=0;	
$outpt1="<form action=\"enter.pl\" method=\"post\"><table align=\"center\">";
@namers=("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten");
@monthnames=("January","February","March","April","May","June","July","August","September","October","November","December");
foreach $i(@fat){
	chomp($i);
	($month, $day, $year, $tvacdays, $pname)=split(/\|/, $i);
	if($name eq $pname){
	    for($fe=0;$fe<12;$fe++){
		 	if($fe==$month){
				$msel.="<option selected value=\"$month\">$monthnames[$month]";
			}else{
				$msel.="<option value=\"$fe\">$monthnames[$fe]";
				}
		}	

		$outpt3.="<tr><td><strong>Day :</strong></td><td><input type=\"text\" name=\"day$namers[$count]\" size=\"2\" value=\"$day\"></td><td><strong>Month :</strong></td><td><select name=\"month$namers[$count]\">$msel</select></td><td><strong>Year :</strong></td><td><input type=\"text\" name=\"year$namers[$count]\" value=\"$year\" size=\"4\"></td><td><strong>Vacation Days :</strong></td><td><input type=\"text\" name=\"vacdays$namers[$count]\" value=\"$tvacdays\" size=\"2\">";
		$outpt3.="</td><td><strong>Erase</strong></td><td><select name=\"del$count\"><option value=\"0\">Yes<option value=\"1\" selected>No</select></td></tr>";
		$count++;
		}
}	
$outpt2.="<tr><td colspan=\"10\" align=\"center\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"hidden\" name=\"name\" value=\"$name\"><input type=\"hidden\" name=\"pass\" value=\"$pass\"><input type=\"hidden\" name=\"count\" value=\"$count\"><input type=\"submit\" Value=\"Submit\">&nbsp;<input type=\"Reset\"></td></tr></table></form>";
if($outpt3 eq ""){
$outpt="<h3><font color=\"#000000\">Sorry $name, you have no entries to modify.</font></h3>"
}else{
$outpt="$outpt1$outpt3$outpt2";
}
getheader();
print <<EndHTML;
<html><head><title>Vacation Schedule for $name</title>@head&nbsp;<br>
<div align="center"><h2><font color="#0000a0">Vacation Schedule for $name<br><img src="/mainpage/images/bluebar.jpg" width="100%" height="5" border="0" alt="blue bar"></font></h2></div>
<br>$outpt
<br><br><a href="/mainpage/businessunits/informationservices/">Return to IS Home Page</a><br><form action="/cgi-bin/payroll/Information%20Services/vaccal2/login.pl" method="post"><input type="hidden" name="name" value="$name"><input type="hidden" name="pass" value="$pass"><input type="submit" value="Back to Selections"></form><br><br>@foot</body></html>
EndHTML
exit;
}else{
getheader();
print <<EndHTML;
<html><head><title>Error</title>@head<br><br>&nbsp;<p><br><br><br>
<h3>Sorry, but the User ID or password is wrong.</h3>
<br><a href="/mainpage/businessunits/informationservices/">Return to IS Home Page</a><br><form action="/cgi-bin/payroll/Information%20Services/vaccal2/login.pl" method="post"><input type="hidden" name="name" value="$name"><input type="hidden" name="pass" value="$pass"><input type="submit" value="Back to Selections"></form><br><br><br><br><br>@foot</body></html>
EndHTML
exit;

}
sub dienice {
	my($msg) = @_;
	print "<h2>Error</h2>\n";
	print $msg;
	exit;
}
sub getheader{
open(INF,"header.dat") or dienice("Couldn't open: $!");					
@head=<INF>;
close(INF);										
	
open(INF,"foot.dat") or dienice("Couldn't open: $!");					
@foot=<INF>;
close(INF);										
	

}	
exit;