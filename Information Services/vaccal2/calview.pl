#module for setting the current working directory
use hrm_homedir;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$value =~ s/\n/ /g;	# replace newlines with spaces
	$value =~ s/\r//g;	# remove hard returns
	$value =~ s/\cM//g;	# delete ^M's
	$FORM{$name} = $value;
}						

$p=$ENV{'QUERY_STRING'};
chomp($p);
	($day, $monthnam, $year)=split(/\|/, $p);
if($day eq "" && $year eq ""){
$day=$FORM{day};
$monthnam=$FORM{selectMonth};
$year=$FORM{txYear};
}
@monthnames=("January","February","March","April","May","June","July","August","September","October","November","December");
@monthn=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec");
for($b=0;$b<12;$b++){
if($monthnam eq $monthnames[$b]){$month=$b;}
}
$month2=$month;
calcdays();
if($startday[0]>($daysinmonths[$month-1]-6)||$startday[0]>($daysinmonths[$month]-6)){

}
if($f!=3 && $zee!=1){
$month2=$month+1;}
open(INF,"input/data.dat") or dienice("Couldn't open: $!");					
@lat=<INF>;
close(INF);										

@daysinmonths=("31","28","31","30","31","30","31","31","30","31","30","31");
for($sat=0;$sat<14;$sat++){
if($month2==$month){
$testmonth[$sat]=$month;
$outmonth[$sat]=$monthn[$month];
$testyear[$sat]=$syear;
}else{
if($fat!=100){
$outmonth[$sat]=$monthn[$month];
$testmonth[$sat]=$month;
$testyear[$sat]=$syear;
if($startday[$sat]==$daysinmonths[$month]){
$fat=100;}
}else{$outmonth[$sat]=$monthn[$month2];
$testmonth[$sat]=$month2;
if($testmonth[$sat]==0){$testyear[$sat]=$eyear;
}else{$testyear[$sat]=$syear;}
}
}
}

$month3=$month-1;
if($month3==-1){$month3=11;}
$outpt1.="<strong><font color=\"#0000a0\">Vacation Calendar for week starting $monthnames[$month] $startday[0], $syear and ending $monthnames[$month2] $startday[13], $eyear</font></strong>";
$outpt1.="<br><br><table width=\"100%\" border=\"1\" align=\"center\"><tr><td align=\"center\" width=\"14%\"><strong><font color=\"#FF0000\">$weekdays[0]<br>$outmonth[0] $startday[0]</font></strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[1]<br>$outmonth[1] $startday[1]</strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[2]<br>$outmonth[2] $startday[2]</strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[3]<br>$outmonth[3] $startday[3]</strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[4]<br>$outmonth[4] $startday[4]</strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[5]<br>$outmonth[5] $startday[5]</strong></td><td align=\"center\" width=\"14%\"><strong><font color=\"#FF0000\">$weekdays[6]<br>$outmonth[6] $startday[6]</font></strong></td></tr>";
$outpt2.="<tr><td align=\"center\" width=\"14%\"><strong><font color=\"#FF0000\">$weekdays[0]<br>$outmonth[7] $startday[7]</font></strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[1]<br>$outmonth[8] $startday[8]</strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[2]<br>$outmonth[9] $startday[9]</strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[3]<br>$outmonth[10] $startday[10]</strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[4]<br>$outmonth[11] $startday[11]</strong></td><td align=\"center\" width=\"14%\"><strong>$weekdays[5]<br>$outmonth[12] $startday[12]</strong></td><td align=\"center\" width=\"14%\"><strong><font color=\"#FF0000\">$weekdays[6]<br>$outmonth[13] $startday[13]</font></strong></td></tr>";

foreach $i(@lat){
	chomp($i);
	($dm,$dd, $dy, $dvac, $name )=split(/\|/, $i);
if(($dm==$month||$dm==$month2||$dm==$month3)&&($dy==$syear||$dy==$eyear||$dy==$syear-1||$dy==$eyear+1)){
for($test=0;$test<$dvac;$test++){
$vacdays[$test]=$dd+$test;
$vacmonths[$test]=$dm;
$vacyear[$test]=$dy;
$dz=$vacmonths[$test];
if($vacdays[$test]>$daysinmonths[$dz]){
$vacdays[$test]=$vacdays[$test]-$daysinmonths[$dm];
$vacmonths[$test]++;
if($vacmonths[$test]==12){
$vacmonths[$test]=0;
$vacyear[$test]++;
}}
for($te=0;$te<14;$te++){
if($testmonth[$te]==$vacmonths[$test]&&$startday[$te]==$vacdays[$test]&&$vacyear[$test]==$testyear[$te]){
$nameday[$te].="<font size=\"-1\">$name</font><br>";
}}
}}}
$outpt1.="<tr><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[0]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[1]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[2]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[3]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[4]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[5]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[6]</strong>&nbsp;</td></tr>";
$outpt2.="<tr><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[7]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[8]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[9]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[10]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[11]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[12]</strong>&nbsp;</td><td align=\"center\" width=\"14%\">&nbsp;<strong>$nameday[13]</strong>&nbsp;</td></tr></table>";
$referer=$ENV{'HTTP_REFERER'};
@passnname = split(/\?/, $referer);
@pass=split(/\|/, $passnname[1]);

getheader();
open(INF,"../emplog.dat") or dienice("Couldn't open: $!");					
@lat=<INF>;
close(INF);										
foreach $i(@lat){
	chomp($i);
	($ka, $ra)=split(/\|/, $i);
if($pass[1] eq $ra && $pass[0] eq $ka){
$setter=12293;
}							
}	
if($setter==12293){
print <<EndHTML;
<html><head><title>IS Vacation Calendar</title>
@head<br><br>$outpt1 $outpt2<p align="right"><form action="/cgi-bin/payroll/Information%20Services/vaccal2/login.pl" method="post"><input type="hidden" name="name" value="$pass[0]"><input type="hidden" name="pass" value="$pass[1]"><input type="submit" value="Back to Selections"></form><br><a href="$ENV{'HTTP_REFERER'}">Back to Calendar</a></p><p>@foot</body></html>
EndHTML
}else{
print <<EndHTML;
<html><head><title>IS Vacation Input</title>$header<br><br>&nbsp;<p>
 
Sorry Wrong user name or password.

<br><br><a href="/mainpage/businessunits/informationservices/vacpayemplogin.shtml">Return to Login Page</a><br><a href="/mainpage/businessunits/informationservices/">Return to IS Home Page</a><br><br><br><br><br><br>$footer</body></html>
EndHTML
exit;
}

sub calcdays{
@timery=localtime(time);
$currday=$timery[3];
$curryear=$timery[5]+1900;
$currweekday=$timery[6];
$currrmonth=$timery[4];
@daysinmonths=("31","28","31","30","31","30","31","31","30","31","30","31");
if($year%4==0 && $month>1){
$totdayinselyr+=1;
}
$eyear=$year;
$syear=$year;
for($xe=0;$xe<$month;$xe++){
$totdayinselyr+=$daysinmonths[$xe];
}
$totdayinselyr+=$day;
$daysgoneinyear=$timery[7];
$diff=$totdayinselyr-($daysgoneinyear+1);
$difyear=$year-$curryear-1;
for($xr=0;$xr<=$difyear;$xr++){
$yeardays+=365;
$v=$curryear+$xr;
if($v%4==0){$yeardays++;}
}
$total=$yeardays+$diff;
$weekdayer=$total%7;
$endweekday=$currweekday+$weekdayer;
if($endweekday>=7){$endweekday-=7;}
@weekdays=("Sun","Mon","Tues","Wed", "Thurs","Fri","Sat");
$startday[0]=$day-$endweekday;
if($startday[0]<=0){

if($month-1==-1){$month2=$month;
				$month+=12;
				$syear=$year-1;
				$zee=1;}
$startday[0]+=$daysinmonths[$month-1];
$month-=1;
}
if($year%4==0 && $month==1){
$daysinmonths[1]=29;
}
for($s=1;$s<=13;$s++){
$startday[$s]=$startday[$s-1]+1;
if($startday[$s]>$daysinmonths[$month]){
if($month==11 && $zee!=1){
$month2=0;
$eyear=$year+1;
$f=3;}

$startday[$s]=1;
}
}
}
sub dienice {
	my($msg) = @_;
	print "<h2>Error</h2>\n";
	print $msg;
	exit;
}
sub getheader{
open(INF,"header.dat") or dienice("Couldn't open: $!");					#opens the admin log file
@head=<INF>;
close(INF);										#closes file

open(INF,"foot.dat") or dienice("Couldn't open: $!");					#opens the admin log file
@foot=<INF>;
close(INF);										#closes file
	

}											#these lines print the error message to the user via a browser
exit;

