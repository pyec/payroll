#module for setting the current working directory
use hrm_homedir;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$value =~ s/\n/ /g;	# replace newlines with spaces
	$value =~ s/\r//g;	# remove hard returns
	$value =~ s/\cM//g;	# delete ^M's
	$FORM{$name} = $value;
}						

$p=$ENV{'QUERY_STRING'};
chomp($p);
	($day, $monthnam, $year, $tvacdays, $pname,$pass)=split(/\|/, $p);
$pname=~ s/%20/ /gi;  
if($day eq ""){
$day=$FORM{day};
$monthnam=$FORM{selectMonth};
$year=$FORM{txYear};
$pass=$FORM{pass};
$pname=$FORM{dr};
$tvacdays=$FORM{daysonvac};
}
$referer=$ENV{'HTTP_REFERER'};
@passnname = split(/\?/, $referer);
@pass=split(/\|/, $passnname[1]);
open(INF,"../emplog.dat") or dienice("Couldn't open: $!");					
@lat=<INF>;
close(INF);										
foreach $i(@lat){
	chomp($i);
	($ka, $ra)=split(/\|/, $i);
if($pass[1] eq $ra && $pass[0] eq $ka){
$setter=12293;
}							
}	
if($setter==12293){
@monthnames=("January","February","March","April","May","June","July","August","September","October","November","December");
for($b=0;$b<12;$b++){
if($monthnam eq $monthnames[$b]){$month=$b;}
}
@timery=localtime(time);
$currday=$timery[3];
$curryear=$timery[5]+1900;
$currweekday=$timery[6];
$currrmonth=$timery[4];
open(INF,"input/data.dat") or dienice("Couldn't open: $!");					
@lat=<INF>;
close(INF);										
							


foreach $i(@lat){
	chomp($i);
	($dm,$dd, $dy, $dvac, $name )=split(/\|/, $i);
if($dy>$curryear||($dy==$currentyear&&$dm>=$currmonth)){
$outpt.="$dm|$dd|$dy|$dvac|$name\n";
}							
}	
							
open(INF,">>input/data.dat") or dienice("Couldn't openh: $!");
print INF "$month|$day|$year|$tvacdays|$pass[0]\n";
close(INF);
@daysinmonths=("31","28","31","30","31","30","31","31","30","31","30","31");
$ld=$day+$tvacdays-1;
$lm=$month;
$ly=$year;
if(($year%4==0 && $year%100!=0)||$year%400==0){$daysinmonths[1]=29;}
if($ld>$daysinmonths[$lm]){
$ld=$ld-$daysinmonths[$lm];
$lm++;
if($lm>=12){
$lm=0;
$ly++;
}
}
getheader();
print <<EndHTML;
<html><head><title>IS Vacation Input for $pname</title>@head<br><br>&nbsp;<p>
Thank You <strong>$pname</strong>, your vacation starts on<strong> $monthnam $day, $year</strong> and ends on <strong>$monthnames[$lm], $ld $ly</strong>. 
<br><br><a href="/mainpage/businessunits/informationservices/">Return to IS Home Page</a><br><form action="/cgi-bin/payroll/Information%20Services/vaccal2/login.pl" method="post"><input type="hidden" name="name" value="$pass[0]"><input type="hidden" name="pass" value="$pass[1]"><input type="submit" value="Back to Selections"></form><br><br><br><br><br>@foot</body></html>
EndHTML
exit;
}else{
getheader();
print <<EndHTML;
<html><head><title>Error</title>@head<br><br>&nbsp;<p><br><br><br>
<h3>Sorry, but the User ID or password is wrong.</h3> 
<br><a href="/mainpage/businessunits/informationservices/">Return to IS Home Page</a><br><a href="/mainpage/businessunits/informationservices/vaccal2/calinput.shtml">Back to Calendar Selection</a><br><br><br><br><br>@foot</body></html>
EndHTML
exit;

}
sub dienice {
	my($msg) = @_;
	print "<h2>Error</h2>\n";
	print $msg;
	exit;
}
sub getheader{
open(INF,"header.dat") or dienice("Couldn't open: $!");					
@head=<INF>;
close(INF);										
								

open(INF,"foot.dat") or dienice("Couldn't open: $!");					
@foot=<INF>;
close(INF);										
	

}	
exit;