### ***********************************************************
### File: user.pl
### Purpose: to validate the login of an employee into the
### 		timesheet application.  See adminlogin.pl for
###			login of administrative users.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for common TimeSheet application utilities
use hrm_timesheet_util;
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;



# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";
#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');

$strLOGIN_PAGE = "/corporateprojects/timesheets/";
$strLOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";

# Get the values submitted by the form
%FORM = getFormValues();
$name = $FORM{'name'};
$pass = $FORM{'pass'};


# Validate the login attempt
if ( validateEmpLogin($name, $pass) == 1) {
	#Login successful, display the menu page
	#printHeader;
	# send the obligatory Content-Type and print the template output
	print "Content-Type: text/html\n\n", $template_header->output;
#   print "HTTP/1.0 200 OK\n";
#   print "Content-Type: text/html\n\n\n";


	print <<EndHTML;

	<p><font color="#0000a0">$strEmpName, what would you like to do?</p>
	
	<form action="userchangetemp.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<select name="choice">
			<option value="Input Hours">Input Hours</option>
			<option value="Modify Profile">Modify Profile</option>
			<option value="View Old Hours">View Old Hours</option>
		</select>
		<input type="submit" value="Submit">
	</form>
	<br>
	
	$strLOGOUT_LINK
	
</body>
</html>

EndHTML


}else{
	#Display a login error
	# send the obligatory Content-Type and print the template output
	print "Content-Type: text/html\n\n", $template_header->output;

	print "
		<h2>Error</h2>
	
		<p>The password or user name is not correct.</p>
	
		<p><a href='http://intranet.halifax.ca/Index.php'>Return to home page</a></p>
	
		<p><a href='http://intranet.halifax.ca/corporateprojects/timesheets/'>Return to Employee Login</a></p>";


	#printEmpLoginErr;
	
}
 
print $template_footer->output;
exit;