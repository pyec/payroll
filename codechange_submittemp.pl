### ***********************************************************
### File: codechange_submit.pl
### Purpose: To change the pay paycode for the specified 
###		division and display the new pay code info
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;

#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');


# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n", $template_header->output;
$strLOGIN_PAGE = "/CorporateProjects/Timesheets/payadmin.html";
$strADM_LOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";


# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$mgr = $FORM{'mgr'};
$div = $FORM{'div'};

$delete_paycode = $FORM{'delete_paycode'};
$cmdDelete = $FORM{'delete'};

$other_paycode = $FORM{'other_paycode'};
$cmdOther = $FORM{'other'};

$add_paycode = $FORM{'add_paycode'};
$add_paycode_desc = $FORM{'add_paycode_desc'};
$cmdAdd = $FORM{'add'};

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, perform the pay code update that was submitted

	# Connect to the local db
	db_connect;

	if ($cmdDelete ne "") {
		### Delete the selected pay code from the specified division
		
		# Delete the pay code for the specified division
		$strSQL = "DELETE FROM Division_PayCodes
					WHERE DivisionId = '$div' AND 
					PayCodeId = '$delete_paycode'";
	
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
		
		# Check if anyone else is using that paycode
		$strSQL = "SELECT DivisionId
					FROM Division_PayCodes
					WHERE PayCodeId = '$delete_paycode'";
	
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

		if (($strUsedByDivId) = $sql_statement->fetchrow_array) {
			# Used by another division, do nothing else
			$strResult = "Pay Code <STRONG>$delete_paycode</STRONG> has been deleted for '$div'.";
		}else{
			# Not used by any other divisions, delete from the master list of pay codes
			$strSQL = "DELETE FROM PayCodes
						WHERE PayCodeId = '$delete_paycode'";
		
			$sql_statement = $conn->prepare($strSQL) 
				or dienice("Can't prepare SQL statement:", $conn->errstr);
			$rsResult = $sql_statement->execute() 
				or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

			$strResult = "Pay Code <STRONG>$delete_paycode</STRONG> has been deleted.";
		}

	}elsif ($cmdOther ne "") {
		### Add the existing pay code to the specified division
		$strSQL = "INSERT INTO Division_PayCodes (DivisionId, PayCodeId) 
					SELECT '$div', '$other_paycode'";
	
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

		$strResult = "Pay Code <STRONG>$other_paycode</STRONG> has been added to '$div'.";

	}elsif ($cmdAdd ne "") {
		### Add a brand new pay code and assign the specified division to it
		# Check that the pay code does not already exist
		$strSQL = "SELECT PayCodeId
					FROM PayCodes
					WHERE PayCodeId = '$add_paycode'";
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
		if (($strAddPayCode) = $sql_statement->fetchrow_array) {
			dienice ("That Pay Code is already in the system. &nbsp;Please go back and enter a different pay code abbreviation.<BR>$strADM_LOGOUT_LINK");
		}

		# Add the pay code to the system
		$strSQL = "INSERT INTO PayCodes (PayCodeId, PayCodeDesc) 
					SELECT '$add_paycode', '$add_paycode_desc'";
	
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

		# Add the pay code to the specified division
		$strSQL = "INSERT INTO Division_PayCodes (DivisionId, PayCodeId) 
					SELECT '$div', '$add_paycode'";
	
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

		$strResult = "Pay Code <STRONG>$add_paycode</STRONG> has been added to the system and is available to '$div'.";

	}else{
		### Unrecognized command
		dienice ("Pay Code function is not recognized.<BR>$strADM_LOGOUT_LINK");
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	#Define instruction string
	$strInstructions = "$strEmpName, the following pay code update has been performed:";
	
#	printHeader;
	print <<EndHTML;
		<font color="#0000a0"><h1 align=center>HRM Payroll Administration</h1></font>
		<p><font color="#0000a0">$strInstructions</p>

		<P>$strResult</P>

		<form action="adminlogintemp.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
			<P><input type="submit" value="Main Menu"></P>
		</FORM>

		$strADM_LOGOUT_LINK
	</BODY>
</HTML>
EndHTML

}else{
	#Display a login error
	printAdmLoginErr;
}

print $template_footer->output;

exit;