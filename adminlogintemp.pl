### ***********************************************************
### File: adminlogin.pl
### Purpose: to validate the login of an administrator into the
### 		timesheet application and display the main menu.
###			See user.pl for login of employees.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for common TimeSheet application utilities
use hrm_timesheet_util;
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;


# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";
#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');


# Get the values submitted by the form
%FORM = getFormValues();
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$adminchoice = $FORM{'adminchoice'};

$strLOGIN_PAGE = "/CorporateProjects/Timesheets/payadmin.html";
$strADM_LOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, display the menu page
		
	# Get the available options based on the user type
	&getChoices;
	
	#printHeader;
	# send the obligatory Content-Type and print the template output
	print "Content-Type: text/html\n\n", $template_header->output;

	print <<EndHTML;
	<font color="#0000a0"><h1 align=center>HRM Payroll Administration Menu</h1></font>
	<p><font color="#0000a0">$strEmpName, what would you like to do?</p>
	
	<form action="adminchoicetemp.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<P><select name="adminchoice">
			$strChoices
		</select></P>
		<input type="submit" value="Submit">
	</form>
	<br>
	
	$strADM_LOGOUT_LINK
	
</body>
</html>

EndHTML

}else{
# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n", $template_header->output;

	#Display a login error
	#printAdmLoginErr;
print "
	<h2>Error</h2>
	
	<p>The password or user name is not correct or the user is not an administrator.</p>
	
	<p><a href='http://intranet.halifax.ca/index.php'>Return to Home Page</a></p>
	
	<p><a href='/corporateprojects/timesheets/payadmin.html'>Return to Administrator Login</a></p>

	<p><a href='/corporateprojects/timesheets/index.html'>Return to Employee Login</a></p>
";

}
print $template_footer->output;

exit;


#######################################################################
### Generate an <OPTION> list of available choices based on the 
###		rights of the current user.
### Possible options are:
###		- Display Hours
###		- Print Hours
###		- Change Pay Period
###		- Manage Users
###		- Manage Pay Codes
###		- View Old Pay Periods
###		- Modify Admin Profile
### OUTPUT:
###	- $strChoices: list of <OPTION> choices HTML code
######################################################################
sub getChoices {
	#Init
	$strChoices = "";
	
	# Common string values
	my($strBegin) = "<OPTION value='";
	my($strMiddle) = "'>";
	my($strEnd) = "</OPTION>\n";
	
	# Define possible options
	# NOTE: Adding an option to this list requires adding a matching 
	#			elsif ($adminchoice eq "???") {
	#		line to the adminchoice.pl script.
	my($strOpt_Display_Hrs) = $strBegin . "display" . $strMiddle . "Display Hours" . $strEnd;
	my($strOpt_Print_Hrs) = $strBegin . "print" . $strMiddle . "Print" . $strEnd;
	my($strOpt_Change_Period) = $strBegin . "advance" . $strMiddle . "Change Pay Period" . $strEnd;
	my($strOpt_Manage_Users) = $strBegin . "users" . $strMiddle . "Manage Users" . $strEnd;
	my($strOpt_Manage_Codes) = $strBegin . "codes" . $strMiddle . "Manage Pay Codes" . $strEnd;
	my($strOpt_Disp_Old_Hrs) = $strBegin . "display_old" . $strMiddle . "View Old Pay Periods" . $strEnd;
	my($strOpt_Modify_Profile) = $strBegin . "profile" . $strMiddle . "Modify Profile" . $strEnd;


	# Select options for each usertype
	if ($strUserType eq "ADM_MGR") {
		$strChoices = $strOpt_Display_Hrs . $strOpt_Change_Period . $strOpt_Manage_Users . 
						$strOpt_Disp_Old_Hrs . $strOpt_Modify_Profile;
	}elsif ($strUserType eq "ADM_BU") {
		$strChoices = $strOpt_Display_Hrs . $strOpt_Change_Period . 
						$strOpt_Manage_Users . $strOpt_Manage_Codes . 
						$strOpt_Disp_Old_Hrs . $strOpt_Modify_Profile;
	}elsif ($strUserType eq "ADM_PRINT") {
		$strChoices = $strOpt_Print_Hrs . $strOpt_Change_Period;
	}elsif ($strUserType eq "ADM_SYS") {
		$strChoices = $strOpt_Display_Hrs . $strOpt_Print_Hrs . $strOpt_Change_Period . 
						$strOpt_Manage_Users . $strOpt_Manage_Codes . 
						$strOpt_Disp_Old_Hrs . $strOpt_Modify_Profile;
	}

}


exit;
