### ***********************************************************
### File: adminchoice.pl
### Purpose: To redirect to the appropriate page based on the
###		previous selection from the admin menu.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$adminchoice = $FORM{'adminchoice'};

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, determine the selection and redirect accordingly
	if($adminchoice eq "display"){
		### Display Hours is selected
		if ($strUserType eq "ADM_BU" or $strUserType eq "ADM_PRINT") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_div.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_bu.pl";
		}else{
			#Administrator for single division, show display hours page
			$strTarget = "adminview.pl";
		}
	}elsif($adminchoice eq "print"){
		### Print Hours is selected
		if ($strUserType eq "ADM_PRINT") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_div.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_bu.pl";
		}else{
			#Other users have no access, return to the menu screen
			$strTarget = "adminlogin.pl";
		}
	}elsif($adminchoice eq "advance"){
		### Change Pay Period is selected
		if ($strUserType eq "ADM_BU" or $strUserType eq "ADM_PRINT") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_div.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_bu.pl";
		}else{
			#Administrator for single division, go immediately to change pay period script
			$strTarget = "adminchange.pl";
		}
	}elsif($adminchoice eq "users"){
		### Change Pay Period is selected
		if ($strUserType eq "ADM_BU") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_div.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_bu.pl";
		}elsif ($strUserType eq "ADM_MGR") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "moduser.pl";
		}else{
			#Other users have no access, return to the menu screen
			$strTarget = "adminlogin.pl";
		}
	}elsif($adminchoice eq "codes"){
		### Change Pay Period is selected
		if ($strUserType eq "ADM_BU") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_div.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_bu.pl";
		}else{
			#Other users have no access, return to the menu screen
			$strTarget = "adminlogin.pl";
		}
	}elsif($adminchoice eq "display_old"){
		### Display Hours is selected
		if ($strUserType eq "ADM_BU") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_div.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_bu.pl";
		}else{
			#Administrator for single division, show select period page
			$strTarget = "admin_sel_per.pl";
		}
	}elsif($adminchoice eq "profile"){
		$strTarget = "adminprofile.pl";
	}else{
		#Unknown choice, return to the menu screen
		$strTarget = "adminlogin.pl";
	}

	#Print the redirect page
	print <<EndHTML;
<html>
<head>
	<title>Time Sheets - Redirecting...</title>
</head>
<body onLoad="document.Redirect.submit();">
<!-- <body> -->
	<form action="$strTarget" method="post" name="Redirect" id="Redirect">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="hidden" name="div" value="$strEmpDivId">
		<input type="hidden" name="bu" value="$strEmpBUId">
		<input type="hidden" name="mgr" value="$strUserId">
		<input type="hidden" name="adminchoice" value="$adminchoice">

<!-- For testing only -->
<!-- <input type="submit" value="Submit"> -->

	</form>
</body>
</html>
EndHTML

}else{
	#Display a login error
	printAdmLoginErr;
}

exit;
