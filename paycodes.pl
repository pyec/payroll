### ***********************************************************
### File: paycodes.pl
### Purpose: To display the paycodes for the division of
###			the current user
### Usage: paycodes.pl?div=XX
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getQueryStringValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%QUERY_STRING = getQueryStringValues();
$divisionId = $QUERY_STRING{'div'};

print <<EndHTML;
<!DOCTYPE html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<TITLE>Time Sheets - List of Pay Codes</TITLE>
</HEAD>

<BODY>
	<P><STRONG>Time Sheets - List of Pay Codes ($divisionId)</STRONG></P>
	<BR>
	<TABLE border="2" cellpadding="2" cellspacing="2">
		<TR>
			<TD bgcolor=EECC66><strong>Pay Code</strong><!-- for Division: $divisionId --></TD>
			<TD bgcolor=EECC66><strong>Description</strong></TD>
		</TR>
EndHTML

	# Connect to the local db
	db_connect;
	
	## Retrieve the Pay Code information
	$strSQL =  "SELECT PayCodeId, PayCodeDesc 
				FROM qrySel_PayCodes 
				WHERE DivisionId = '$divisionId'
				ORDER BY PayCodeId";
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement: ", $conn->errstr);
	
	while (($paycode_id, $paycode_desc) = $sql_statement->fetchrow_array) {
		print "<TR>\n<TD>$paycode_id</TD>\n 
					<TD>$paycode_desc</TD>\n<TR>\n";
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;
	
	## ********************************
print <<EndHTML;
	</TABLE>
</BODY>
</HTML>
EndHTML

exit;