### ***********************************************************
### File: payperiod.pl
### Purpose: To display the payperiods for the current year
### Usage: payperiod.pl?year=####&uid=xxxxxxxx
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getQueryStringValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%QUERY_STRING = getQueryStringValues();
$nYear = $QUERY_STRING{'year'};
$strUserId = $QUERY_STRING{'uid'};

print <<EndHTML;
<!DOCTYPE html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<TITLE>Time Sheets - List of Pay Periods</TITLE>
</HEAD>

<BODY>
	<P align=center><STRONG>Time Sheets - List of Pay Periods ($nYear)</STRONG></P>
	<TABLE border="2" cellpadding="2" cellspacing="2">
		<TR>
			<TD bgcolor=EECC66><strong>Pay Period</strong></TD>
			<TD bgcolor=EECC66><strong>Actual Pay Day</strong></TD>
			<TD bgcolor=EECC66><strong>Pay Period #</strong></TD>
		</TR>
EndHTML

	# Connect to the local db
	db_connect;
	
	### Retrieve the supervisor for current user
	$strSQL = "SELECT SupervisorId
				FROM Users
				WHERE UserId = '$strUserId'";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	($strMgrId) = $sql_statement->fetchrow_array;
	if ($strMgrId eq "") {
		# If no supervisor, then user is likely own supervisor
		$strMgrId = $strUserId;
	}

	### Retrieve the current pay period for the current user
	$strSQL = "SELECT CurrentPeriodId
				FROM Supervisors
				WHERE SupervisorId = '$strMgrId'";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	($strCurrentPeriodNum) = $sql_statement->fetchrow_array;

	### Retrieve the Pay Period information
	$strSQL =  "SELECT PeriodId, PeriodStartDate, PeriodEndDate, PeriodPayDate
				FROM Periods
				WHERE Year = $nYear
				ORDER BY PeriodId";
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	while (($strPeriodId, $strStartDate, $strEndDate, $strPayDate) 
				= $sql_statement->fetchrow_array) {
		#Format the dates
		$strPeriodDates = dateToString($strStartDate);
		$strPeriodDates .= " - ";
		$strPeriodDates .= dateToString($strEndDate);
		$strPayDay = dateToString($strPayDate);

		# Determine if it is the current period
		if ($strPeriodId eq $strCurrentPeriodNum) {
			# Current Period, Bold the line and fill in the 'Current' Column
			$strPeriodDates = "<STRONG>$strPeriodDates</STRONG>";
			$strPayDay = "<STRONG>$strPayDay</STRONG>";
			$strPeriodId = "<STRONG>$strPeriodId</STRONG>";
			$strBGColour = " bgcolor='#DDDDFF'";
		}else{
			# Not Current Period, do not highlight the row
			$strBGColour = "";
		}

		#Display the table row
		print "<TR>\n<TD$strBGColour><a name='$strPeriodId'></a>$strPeriodDates</TD>\n
				<TD$strBGColour>$strPayDay</TD>\n
				<TD$strBGColour>$strPeriodId</TD>\n<TR>\n";
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;
	
	## ********************************
print <<EndHTML;
	</TABLE>
</BODY>
</HTML>
EndHTML

exit;