### ***********************************************************
### File: adpassmod.pl
### Purpose: To update the admin profile in the db
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name=$FORM{'name'};
$pass=$FORM{'pass'};
$newpass1=$FORM{'newpass1'};
$newpass2=$FORM{'newpass2'};
$uname=$FORM{'uname'};


# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, display the confirmation page
	updateProfile();
	
	#if there is a username, display it
	if($uname ne ""){
		$unamehtml=" and your user name is '$uname'.";
	}else{
		$unamehtml=".";
	}

	printHeader;
	print <<EndHTML;

	<P><font color="#0000a0">Thank You $strEmpName. Your Password is now '$newpass1'$unamehtml</P>
	<FORM action='adminlogin.pl' method='post'>
		<input type='hidden' name='userid' value='$strUserId'>
		<input type='hidden' name='name' value='$name'>
		<input type='hidden' name='pass' value='$newpass1'>
		<input type="submit" ID="hiddenButton" value="Main Menu">	
	</FORM>
	$strADM_LOGOUT_LINK

</body>
</html>
EndHTML

}else{
	#Display a login error
	printAdmLoginErr;
}

exit;

######################################################################
## Update the password and login for the user profile in the db
######################################################################
sub updateProfile {
	# Connect to the local db
	db_connect;
	
	# Check that the same login does not already exist
	$strSQL = "SELECT UserId, Login
				FROM Users
				WHERE UserId <> '$strUserId' AND 
					(Login = '$uname')";
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

	#Do not continue if that Login is already in use
	if (($strCurrentUser, $strCurrentLogin) = $sql_statement->fetchrow_array) {
		if ($strCurrentLogin eq $uname) {
			dienice ("That Login is already in use. &nbsp;Please go back and enter a different Login.<BR>$strADM_LOGOUT_LINK");
		}
	}

	# Update the user record with the specified unique data
	$strSQL = "UPDATE Users 
				SET Login = ?, Password = ?
				WHERE UserId = ?";
	$sql_statement = $conn->prepare($strSQL)
		or dienice("Can't prepare SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	$rsResult = $sql_statement->execute($uname, $newpass1, $strUserId)
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;
}

