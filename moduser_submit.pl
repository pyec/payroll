### ***********************************************************
### File: moduser_submit.pl
### Purpose: To add/delete/modify the specified 
###		user and display the new info
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$mgr = $FORM{'mgr'};

$delete_user = $FORM{'delete_user'};
$cmdDelete = $FORM{'fn_delete'};

$upd_user = $FORM{'upd_user'};
$upd_login = $FORM{'upd_login'};
$upd_emp_name = $FORM{'upd_emp_name'};
$upd_emp_num = $FORM{'upd_emp_num'};
$upd_status = $FORM{'upd_status'};
$upd_pwd = $FORM{'upd_pwd'};
$cmdUpdate = $FORM{'fn_update'};

$add_login = $FORM{'add_login'};
$add_emp_name = $FORM{'add_emp_name'};
$add_emp_num = $FORM{'add_emp_num'};
$add_status = $FORM{'add_status'};
$add_pwd = $FORM{'add_pwd'};
$cmdAdd = $FORM{'fn_add'};

# Defaults for add new user
$strDEFAULT_USER_TYPE = "EMP";
$strDEFAULT_DIV = "NONE";

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, perform the user update that was submitted

	# Connect to the local db
	db_connect;

	#Init the result string
	$strResult = "";

	if ($cmdDelete ne "") {
		### Delete the selected user
		$strSQL = "DELETE FROM Users
					WHERE UserId = '$delete_user'";
	
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
		
		$strResult = "User <STRONG>$delete_user</STRONG> has been deleted.";

	}elsif ($cmdUpdate ne "") {
		### Update the existing user data
		# Check that the same login or username does not already exist
		$strSQL = "SELECT UserId, Login, EmployeeName
					FROM Users
					WHERE UserId <> '$upd_user' AND 
						(Login = '$upd_login' OR EmployeeName = '$upd_emp_name')";
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
		if (($strCurrentUser, $strCurrentLogin, $strCurrentName) = $sql_statement->fetchrow_array) {
			if ($strCurrentLogin eq $upd_login) {
				dienice ("That Login is already in use. &nbsp;Please go back and enter a different Login.<BR>$strADM_LOGOUT_LINK");
			}
			if ($strCurrentName eq $upd_emp_name) {
				dienice ("That Name is already in use. &nbsp;Please go back and enter a different Name.<BR>Adding a middle initial or the division name after the name may help.<BR>$strADM_LOGOUT_LINK");
			}
		}
		
		# Update the user
		$strSQL = "UPDATE USERS
					SET Login = '$upd_login', 
					EmployeeName = '$upd_emp_name', 
					EmployeeNumber = $upd_emp_num, 
					StatusId = '$upd_status'";
					
		# Update the password if it is not blank
		if ($upd_pwd ne "") {
			$strSQL .= ", Password = '$upd_pwd' ";
		}
		$strSQL .= " WHERE UserId = '$upd_user'";
	
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:<BR>$strSQL", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

		$strResult = "User <STRONG>$upd_emp_name</STRONG> has been updated.";

		# If the user has changed their own password through this means, then
		# pass the new credentials to subsequent pages.
		if ($upd_user eq $strUserId and $upd_pwd ne "") {
			$pass = $upd_pwd;
		}
	}elsif ($cmdAdd ne "") {
		### Add a brand new user and assign the specified division to it
		# Check that the same login or username does not already exist
		$strSQL = "SELECT UserId, Login, EmployeeName
					FROM Users
					WHERE UserId = '$add_login' OR 
						Login = '$add_login' OR 
						EmployeeName = '$add_emp_name'";
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
		if (($strCurrentUser, $strCurrentLogin, $strCurrentName) = $sql_statement->fetchrow_array) {
			if ($strCurrentUser eq $upd_login or $strCurrentLogin eq $upd_login) {
				dienice ("That Login is already in use. &nbsp;Please go back and enter a different Login.<BR>$strADM_LOGOUT_LINK");
			}
			if ($strCurrentName eq $upd_emp_name) {
				dienice ("That Name is already in use. &nbsp;Please go back and enter a different Name.<BR>
							Adding a middle initial or the division name after the name may help.<BR>$strADM_LOGOUT_LINK");
			}
		}

		#Get the division of the mgr for the user
		$strSQL = "SELECT DivisionId
					FROM Users
					WHERE UserId = '$mgr'";
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
		($strDiv) = $sql_statement->fetchrow_array;
		
		# Validate the division was retrieved
		if ($strDiv eq "") {
			$strDiv = $strDEFAULT_DIV;
		}
		
		# Add the user to the system
		$strSQL = "INSERT INTO Users 
						(UserId, Login, Password, 
						EmployeeName, EmployeeNumber, SupervisorId, 
						DivisionId, UserTypeId, StatusId) 
					SELECT '$add_login', '$add_login', '$add_pwd', 
						'$add_emp_name', $add_emp_num, '$mgr', 
						'$strDiv', '$strDEFAULT_USER_TYPE', '$add_status'";
	
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);


		$strResult = "User <STRONG>$add_emp_name</STRONG> has been added to the system.";

	}else{
		### Unrecognized command
		dienice ("User function is not recognized.<BR>$strADM_LOGOUT_LINK");
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	#Define instruction string
	$strInstructions = "$strEmpName, the following user update has been performed:";
	
	printHeader;
	print <<EndHTML;
		<font color="#0000a0"><h1 align=center>HRM Payroll Administration</h1></font>
		<p><font color="#0000a0">$strInstructions</p>

		<P>$strResult</P>

		<form action="moduser.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
			<input type="hidden" name="mgr" value="$mgr">
			<P><input type="submit" value="Continue to Manage Users"></P>
		</FORM>

		<form action="adminlogin.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
			<P><input type="submit" value="Main Menu"></P>
		</FORM>
		
		$strADM_LOGOUT_LINK
	</BODY>
</HTML>
EndHTML

}else{
	#Display a login error
	printAdmLoginErr;
}

exit;