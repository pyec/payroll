### ***********************************************************
### File: adminchoice.pl
### Purpose: To redirect to the appropriate page based on the
###		previous selection from the admin menu.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;

### Local modules:
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');


#print "Content-type:text/html\n\n";

 

# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n", $template_header->output;

$strLOGIN_PAGE = "/CorporateProjects/Timesheets/payadmin.html";
$strADM_LOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";


# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$adminchoice = $FORM{'adminchoice'};

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, determine the selection and redirect accordingly
	if($adminchoice eq "display"){
		### Display Hours is selected
		if ($strUserType eq "ADM_BU" or $strUserType eq "ADM_PRINT") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_divtemp.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_butemp.pl";
		}else{
			#Administrator for single division, show display hours page
			$strTarget = "adminviewtemp.pl";
		}
	}elsif($adminchoice eq "print"){
		### Print Hours is selected
		if ($strUserType eq "ADM_PRINT") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_divtemp.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_butemp.pl";
		}else{
			#Other users have no access, return to the menu screen
			$strTarget = "adminlogintemp.pl";
		}
	}elsif($adminchoice eq "advance"){
		### Change Pay Period is selected
		if ($strUserType eq "ADM_BU" or $strUserType eq "ADM_PRINT") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_divtemp.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_butemp.pl";
		}else{
			#Administrator for single division, go immediately to change pay period script
			$strTarget = "adminchangetemp.pl";
		}
	}elsif($adminchoice eq "users"){
		### Change Pay Period is selected
		if ($strUserType eq "ADM_BU") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_divtemp.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_butemp.pl";
		}elsif ($strUserType eq "ADM_MGR") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "modusertemp.pl";
		}else{
			#Other users have no access, return to the menu screen
			$strTarget = "adminlogintemp.pl";
		}
	}elsif($adminchoice eq "codes"){
		### Change Pay Period is selected
		if ($strUserType eq "ADM_BU") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_divtemp.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_butemp.pl";
		}else{
			#Other users have no access, return to the menu screen
			$strTarget = "adminlogintemp.pl";
		}
	}elsif($adminchoice eq "display_old"){
		### Display Hours is selected
		if ($strUserType eq "ADM_BU") {
			#Administrator for multiple divisions, display select division page
			$strTarget = "admin_sel_divtemp.pl";
		}elsif ($strUserType eq "ADM_SYS") {
			#Administrator for multiple BUs, display select BU page
			$strTarget = "admin_sel_butemp.pl";
		}else{
			#Administrator for single division, show select period page
			$strTarget = "admin_sel_pertemp.pl";
		}
	}elsif($adminchoice eq "profile"){
		$strTarget = "adminprofiletemp.pl";
	}else{
		#Unknown choice, return to the menu screen
		$strTarget = "adminlogintemp.pl";
	}

	#Print the redirect page
	print <<EndHTML;
 

 
<!-- <body> -->
	<form action="$strTarget" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="hidden" name="div" value="$strEmpDivId">
		<input type="hidden" name="bu" value="$strEmpBUId">
		<input type="hidden" name="mgr" value="$strUserId">
		<input type="hidden" name="adminchoice" value="$adminchoice">

<!-- For testing only -->
<!-- <input type="submit" value="Submit"> -->

	</form>




EndHTML

 print " <script language=JavaScript>document.forms[1].submit();</script>"; 


}else{
	#Display a login error
	printAdmLoginErr;

}

print $template_footer->output;
exit;
