### ***********************************************************
### File: adminchange_submit.pl
### Purpose: To change the pay period for the specified 
###		supervisor and display the new pay period info
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$mgr = $FORM{'mgr'};
$period = $FORM{'new_period'};

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, change the current period and display the new info

	# Connect to the local db
	db_connect;
	
	### Update the CURRENT pay period for the specified supervisor
	$strSQL = "UPDATE Supervisors
				SET CurrentPeriodId = '$period'
				WHERE SupervisorId = '$mgr'";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	# Get period start, end and pay dates in string form
	getPeriodDates($period);

	#Define instruction string
	$strInstructions = "$strEmpName, the pay period has been updated to:";
	
	printHeader;
	print <<EndHTML;
		<font color="#0000a0"><h1 align=center>HRM Payroll Administration</h1></font>
		<p><font color="#0000a0">$strInstructions</p>

		<form action="adminlogin.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
	
		<TABLE cellpadding="2" border="1">
			<TR>
				<TD bgcolor='#eecc66' align=middle><STRONG>Current Period</STRONG></TD>
			</TR>
			<TR>
				<TD>
					<strong>PeriodID: $period</strong><BR>
					Start Date: $strStartDate<BR>
					End Date: $strEndDate<BR>
					Pay Day: $strPayDate
				</TD>
			</TR>
		</TABLE>

		<P><input type="submit" value="Main Menu"></P>
		</FORM>

		$strADM_LOGOUT_LINK
	</BODY>
</HTML>
EndHTML

}else{
	#Display a login error
	printAdmLoginErr;
}

exit;