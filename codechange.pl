### ***********************************************************
### File: codechange.pl
### Purpose: To display a picklist of pay codes to delete
###		or accept entry of a new pay code
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$mgr = $FORM{'mgr'};

#Default the mgr to the current user if mgr is not specified
if ($mgr eq "") {
	$mgr = $strUserId;
}

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, display the pay code information

	### Get the pay codes for the division for the specified manager
	# Connect to the local db
	db_connect;

	# Get the division for the specified manager
	$strSQL = "SELECT DivisionId
				FROM Users
				WHERE UserId = '$mgr'";
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

	($strDivId) = $sql_statement->fetchrow_array;
	
	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	if ($strDivId ne "") {
		# Get the paycodes for the specified divsion
		getCodes ($strDivId);
		
	}else{
		dienice("Can't locate Division $strDivId in the database.\n", $conn->errstr);
		exit;
	}
	
	### Get pay codes that exist but are not used by the specified division
	# Connect to the local db
	db_connect;

	# Get the complete list of distinct paycodes
	$strSQL = "SELECT PayCodeId, PayCodeDesc
				FROM PayCodes
				WHERE PayCodeId NOT IN 
					(SELECT PayCodeId FROM Division_PayCodes
					WHERE DivisionId = '$strDivId')";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

	# Store the pay code ids and descriptions into arrays
	my($nCodeCount) = 0;
	while (($paycode_id, $paycode_desc) = $sql_statement->fetchrow_array) {
		# Save the paycodes that are not already in @aPayCodeId
		$aOtherPayCodeId[$nCodeCount] = $paycode_id;
		$aOtherPayCodeDesc[$nCodeCount++] = $paycode_desc;
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;

	# Display the page
	printHeader;
	print <<EndHTML;

	<P><font color="#0000A0">$strEmpName, please perform one of the following Pay Code functions:</font></P>
	
	<form action="codechange_submit.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="hidden" name="mgr" value="$mgr">
		<input type="hidden" name="div" value="$strDivId">

	<TABLE border='2' align='center' cellpadding='5'>
		<TR>
			<TD bgcolor='EECC66'><strong>Delete Pay Code:</strong></TD>
			<TD bgcolor='EECC66'><strong>Use Other Existing Pay Codes:</strong></TD>
			<TD bgcolor='EECC66'><strong>Add New Pay Code:</strong></TD>
		</TR>
		<TR>
			<TD><P>Pay Code:<BR>
				<SELECT name='delete_paycode'>
EndHTML
	for ($nPayCodeCount=0; $nPayCodeCount < @aPayCodeId; $nPayCodeCount++) {
		print ("					<OPTION value='$aPayCodeId[$nPayCodeCount]'>$aPayCodeId[$nPayCodeCount] - $aPayCodeDesc[$nPayCodeCount]</OPTION>\n");
	}
	print <<EndHTML;
				</SELECT><BR></P>
				<P align='center'><INPUT type='submit' value='Delete' name='delete'></P>
			</TD>
			<TD><P>Pay Code:<BR>
EndHTML
	$nOtherCount = @aOtherPayCodeId;
	if ($nOtherCount > 0) {
		print ("				<SELECT name='other_paycode'>\n");
		for ($nPayCodeCount=0; $nPayCodeCount < @aOtherPayCodeId; $nPayCodeCount++) {
			print ("					<OPTION value='$aOtherPayCodeId[$nPayCodeCount]'>$aOtherPayCodeId[$nPayCodeCount] - $aOtherPayCodeDesc[$nPayCodeCount]</OPTION>\n");
		}
		print ("				</SELECT>\n");
	}else{
		print ("&nbsp;&nbsp;<font color='#C0C0C0'>[None Available]</font>\n");
	}
	print <<EndHTML;

				<BR></P>
				<P align='center'><INPUT type='submit' value='Add Other' name='other'></P>
			</TD>
			<TD><P>Pay Code: <INPUT maxlength='5' size='5' name='add_paycode' onChange='this.value=this.value.toUpperCase()'><BR>
				Description: <INPUT maxlength='40' name='add_paycode_desc'></P>
				<P align='center'><INPUT type='submit' value='Add New' name='add'></P>
			</TD>
		</TR>
	</TABLE>

	</form>

	<P align='center'><form action="adminlogin.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="submit" value="Main Menu">
	</form></P>

	$strADM_LOGOUT_LINK
</BODY>
</HTML>
EndHTML

}else{
	#Display a login error
	printAdmLoginErr;
}

exit;

sub delco{
	## Open the log file
	open(F_DEL_LOG,">delete_code.log") or dienice("Couldn't open: delete_code.log $!");
	print F_DEL_LOG "Deleting code: $delcode - STARTING\n";

	## Remove the code from each employee file ##
	@filearray=("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26");
	## Get the list of administrators
	open(INF,"adlog.dat") or dienice("Couldn't open(5): adlog.dat $!");
	@mu=<INF>;
	close(INF);		

	foreach $i(@mu){
		chomp($i);
		($name1, $pass1, $rights)=split(/\|/, $i);
		if($rights eq "Print"){
			## Get the list of employees for each administrator
			open(INF,"payperiod/personell/$name1/emplog.dat") or dienice("Couldn't open(6): payperiod/personell/$name1/emplog.dat $!");
			@su=<INF>;
			close(INF);		#closes file

			print F_DEL_LOG "-Deleting codes for administrator: $name1\n";

			foreach $i(@su){
				chomp($i);
				($name122, $pass122, $num122, $empnum122, $dept22)=split(/\|/, $i);
				
				print F_DEL_LOG " -Deleting codes for employee: $name122\n";

				## For each pay period for each employee, remove the code
				for($n=0;$n<=25;$n++){
					print F_DEL_LOG "  -Deleting codes for period: $n\n";

					$fileoutpt="";
					$monthy=$filearray[$n];
					open(INF,"payperiod/personell/$name1/$monthy/$name122.dat") or dienice("Couldn't open(7):personell/$name1/$monthy/$name122.dat $!");
					@def=<INF>;
					close(INF);
					$zee=0;
					foreach $i(@def){
						## Find the line in the .dat file for the specified code and remove it
						chomp($i);
						($co, $ho)=split(/\|/, $i);
						$zee++;
		
						if($regtoaddon>0 && "$co" eq "REG" && $zee>=42){
							$ho+=$regtoaddon;
						}
		
						if("$co" ne "$delcode"){
							$fileoutpt.="$co|$ho\n";				
						}elsif("$co" eq "$delcode" && $zee<42){
							$fileoutpt.="REG|$ho\n";
							$regtoaddon+=$ho;
						}
					}
					open(INF,">payperiod/personell/$name1/$monthy/$name122.dat") or logerror();
					if($bError eq 0) {
						print INF "$fileoutpt";
						close(INF);
					}else{
						#Clear the error
						print F_DEL_LOG "   -ERROR: Couldn't Open: payperiod/personell/$name1/$monthy/$name122.dat - $!\n";
						$bError = 0;
					}
				}	
			}	
		}
	}

	## Remove the code from the code file ##
	print F_DEL_LOG "-Deleting code from codes.dat\n";
	$counter=-1;
	open(INF,"codes.dat") or dienice("Couldn't open(9): codes.dat $!");
	@wet=<INF>;
	close(INF);
	foreach $i(@wet){
		chomp($i);
		($cy)=split(/\|/, $i);
		$counter++;
		if("$cy" ne "$delcode"){
			if($cy eq $counter){$cy-=1;}
			$output2.="$cy\n";		
		}else{
			print F_DEL_LOG " -Code removed\n";
		}

		$htmloutput="$output2";
		open(INF,">codes.dat") or dienice("Couldn't open(10): codes.dat $!");
		print INF "$output2";
		close(INF);
	}

	## Close the delete_code.log file
	print F_DEL_LOG "Deleting code: $delcode - COMPLETE\n";
	close(F_DEL_LOG);

	#Display the contents of the log file
#	open(INF,"delete_code.log") or dienice("Couldn't open(10b): delete_code.log $!");
#	@F_LOG_FILE=<INF>;
#	close(INF);		

#	$fileoutpt="";
#	foreach $strLogEntry(F_LOG_FILE){
#		chomp($strLogEntry);
#		$fileoutpt.="$strLogEntry\n";
#	}
	$fileoutpt="Please see the log file delete_code.log for details.";

}

sub addco{
	## Open the log file
	open(F_ADD_LOG,">add_code.log") or dienice("Couldn't open: add_code.log $!");
	print F_ADD_LOG "Adding code: $addcode - STARTING\n";

	## Add the new code to the code file ##
	print F_ADD_LOG "-Adding code to codes.dat\n";
	$counter=0;
	$bet=0;
	open(INF,"codes.dat") or dienice("Couldn't open(11): codes.dat $!");
	@fCodeFile=<INF>;
	close(INF);
	foreach $i(@fCodeFile){
		chomp($i);
		($cr)=split(/\|/, $i);
		if($fCodeFile[$bet] != $counter || $counter==0){
			$output2.="$fCodeFile[$bet]\n";
		}else{
			$fCodeFile[$bet]+=1;
			$output2.="$addcode\n$fCodeFile[$bet]\n";
			print F_ADD_LOG " -Insertion point found\n";
		}

		$counter++;
		$bet++;
	}
	open(INF,">codes.dat") or dienice("Couldn't open(12): codes.dat $!");
	print INF "$output2";
	print F_ADD_LOG " -Code added\n";
	close(INF);		

	## Add the new code to each employee file ##
	## Get the list of administrators
	open(INF,"adlog.dat") or dienice("Couldn't open(13): adlog.dat $!");
	@mu=<INF>;
	close(INF);
	foreach $i(@mu){
		chomp($i);
		($name1, $pass1, $rights)=split(/\|/, $i);
		if($rights eq "Print"){
			## Get the list of employees for each administrator
			open(INF,"payperiod/personell/$name1/emplog.dat") or dienice("Couldn't open(14): payperiod/personell/$name1/emplog.dat $!");
			@su=<INF>;
			close(INF);		#closes file

			print F_ADD_LOG "-Adding codes for administrator: $name1\n";

			foreach $i(@su){
				chomp($i);
				($name122, $pass122, $num122, $empnum122, $dept22)=split(/\|/, $i);
				$fileoutpt="";

				print F_ADD_LOG " -Adding codes for employee: $name122\n";
				for($n=1;$n<=26;$n++){
					print F_ADD_LOG "  -Adding codes for period: $n\n";
					## For each pay period for each employee, add the new code
					$monthy=$n;
					$outie="";
					open(INF,"payperiod/personell/$name1/$monthy/$name122.dat") or dienice("Couldn't open(15): personell/$name1/$monthy/$name122.dat $!");
					@def=<INF>;
					close(INF);

					foreach $i(@def){
						## Find the Total| line in the .dat file and insert the new paycode before it
						chomp($i);
						($co, $ho)=split(/\|/, $i);
						if($co eq "Total"){
							$outie.="$addcode|\n$co|$ho\n";
						}else{
							$outie.="$co|$ho\n";
						}
					}

					open(INF,">payperiod/personell/$name1/$monthy/$name122.dat") or logerror();
					if($bError eq 0) {
						print INF "$outie";
						close(INF);
					}else{
						#Clear the error
						print F_ADD_LOG "   -ERROR: Couldn't Open: payperiod/personell/$name1/$monthy/$name122.dat - $!\n";
						$bError = 0;
					}
				}
			}
		}
	}

	## Close the add_code.log file
	print F_ADD_LOG "Adding code: $addcode - COMPLETE\n";
	close(F_ADD_LOG);

	#Display the contents of the log file
#	open(INF,"add_code.log") or dienice("Couldn't open(16): add_code.log $!");
#	@F_LOG_FILE=<INF>;
#	close(INF);		

#	$fileoutpt="";
#	foreach $strLogEntry(F_LOG_FILE){
#		chomp($strLogEntry);
#		$fileoutpt.="$strLogEntry\n";
#	}
	$fileoutpt="Please see the log file add_code.log for details.";

}

exit;