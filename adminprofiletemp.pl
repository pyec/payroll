### ***********************************************************
### File: adminprofile.pl
### Purpose: To display the admin profile in edit mode.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;

#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');


# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n", $template_header->output;

$strLOGIN_PAGE = "/CorporateProjects/Timesheets/payadmin.html";
$strADM_LOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";


# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$adminchoice = $FORM{'adminchoice'};

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, determine the selection and redirect accordingly
		#Display the admin profile for editing
#		printHeader;
		print <<EndHTML;
			<p><font color="#0000a0">Welcome $strEmpName.  Your Profile is as follows:</p>

			<FORM onSubmit="return checkPwd();" action="adpassmodtemp.pl" method="post">
				<input type="hidden" name="userid" value="$strUserId">
				User Name:<input type="text" name="uname" value="$strLogin"><br>
				Name : $strEmpName<input type="hidden" name="name" value="$strEmpName"><br>
				Business Unit : $strEmpBU<input type="hidden" name="bu" value="$strEmpBU"><br>
				Division : $strEmpDiv<input type="hidden" name="division" value="$strEmpDiv"><br>
				Supervisor : $strEmpMgr<input type="hidden" name="mgr" value="$strEmpMgr"><br>
				Employee Number : $strEmpNum<input type="hidden" name="empnum" value="$strEmpNum"><br>
				Old Password : $pass<input type="hidden" name="pass" value="$pass"><br>
				New Password : <input type="password" name="newpass1" value="$pass"><br>
				Confirm New Password : <input type="password" name="newpass2" value="$pass"><br>
				<input type="SUBMIT" value="Submit"><input type="Reset" value="Reset">
			</form>

			<FORM action='adminlogintemp.pl' method='post' name='menu'>
				<input type='hidden' name='userid' value='$strUserId'>
				<input type='hidden' name='name' value='$name'>
				<input type='hidden' name='pass' value='$pass'>
				<input type="submit" ID="hiddenButton" value="Main Menu">
			</FORM>
			$strADM_LOGOUT_LINK
	</body>
</html>
EndHTML
print $template_footer->output;
		exit;
}else{
	#Display a login error
	printAdmLoginErr;
}
print $template_footer->output;
exit;

