### ***********************************************************
### File: displayhours.pl
### Purpose: To display the hours for the period specified
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
# Module for manipulating dates
use Date::Calc qw( Add_Delta_Days 
	                Month_to_Text);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name=$FORM{'name'};
$pass=$FORM{'pass'};
$period=$FORM{'period'};


# Validate the login attempt
if ( validateEmpLogin($name, $pass) == 1) {
	#Login successful, dislpay the data

	# Get period start, end and pay dates in string form
	getPeriodDates($period);

	printHeader;
	print <<EndHTML;

	<FONT color="#0000a0"><h1 align="center">HRM Attendance Tracking & Payroll Reimbursement Page</h1></FONT>

	<P align="center"><FONT color="#0000a0">Welcome $strEmpName.</FONT></P>
	
	<P><FONT color="#0000a0"><STRONG>Pay Period : $period<BR>
		Start Date : $strStartDate<BR>
		End Date : $strEndDate<BR>
		Pay Day : $strPayDate</STRONG></FONT></P>
	
	<INPUT type='button' value='Pay Code Chart' onClick="window.open('paycodes.pl?div=$strEmpDivId','mywindow','width=450,height=200,scrollbars=yes,resizable=yes')">

	<FORM name="chart" action="user.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type=hidden name="name" value="$name">
		<input type=hidden name="pass" value="$pass">
		<input type=hidden name="period" value="$period">

EndHTML

	# Connect to the local db once to reduce overhead and improve performance
	db_connect;
	
	### Start table of hours
	print ("	<table align=center cellspacing=5 cellpadding=5 border=0 frame='box'>\n");

	### Display column headers with weekdays and dates for first week
	print ("		<tr>\n");
	for($dailycount=0;$dailycount<7;$dailycount++){
		#calculate the date to display
		@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount);
		$strCurrentDate = sprintf("%3.3s %s", Month_to_Text($aCurrentDate[1]), $aCurrentDate[2]);

		#print the html
		print ("			<td align=center><FONT color='#0000a0'><STRONG>$strWEEKDAYS[$dailycount] <br><font style='text-decoration: underline;'>$strCurrentDate</font></STRONG></FONT></td>\n");
	}
	print ("		</tr>\n");

	### Display first week of codes and hours
	print ("		<tr>\n");
	# Display the cells for week #1
	printWeekHours (1);
	print ("		</tr>\n");


	### Display column headers with weekdays and dates for second week (first week + 7)
	print ("		<tr>\n");
	for($dailycount=0;$dailycount<7;$dailycount++){
		#calculate the date to display
		@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount + 7);
		$strCurrentDate = sprintf("%3.3s %s", Month_to_Text($aCurrentDate[1]), $aCurrentDate[2]);

		#print the html
		print ("			<td align=center><FONT color='#0000a0'><STRONG>$strWEEKDAYS[$dailycount] <br><font style='text-decoration: underline;'>$strCurrentDate</font></STRONG></FONT></td>\n");
	}
	print ("		</tr>\n");

	### Display second week of codes and hours
	print ("		<tr>\n");
	# Display the cells for week #2
	printWeekHours (2);
	print ("		</tr>\n");

	## Disconnect from the local Access db after repeated calls to getHours()
	#$sql_statement->finish;
	$conn->disconnect;

	### Display Totals
	print ("		<tr>\n");
	# Display total hours
	$strTotal = 0;
	print ("			<td colspan=3 valign=top><FONT color='#0000a0'><STRONG><font style='text-decoration: underline;'>Total Hours:</font></STRONG></FONT><BR>\n");
	foreach $keys (keys %TotalHrs) {
		print ("				<STRONG>$keys: $TotalHrs{$keys}</STRONG><BR>\n");
		$strTotal += $TotalHrs{$keys};
	}
	print ("				&nbsp;<BR>\n");
	print ("				<STRONG><FONT color='#0000a0'>Total: $strTotal Hrs</FONT></STRONG>\n");
	print ("			</td>\n");

	# Display total dollars
	$strTotal = 0;
	print ("			<td colspan=4 valign=top><FONT color='#0000a0'><STRONG><font style='text-decoration: underline;'>Total Dollars:</font></STRONG></FONT><BR>\n");
	foreach $keys (keys %TotalDlrs) {
		$strDollars = sprintf("\$%.2f", $TotalDlrs{$keys});
		print ("				<STRONG>$keys: $strDollars</STRONG><BR>\n");
		$strTotal += $TotalDlrs{$keys};
	}
	print ("				&nbsp;<BR>\n");
	$strTotal = sprintf("\$%.2f", $strTotal);
	print ("				<STRONG><FONT color='#0000a0'>Total: $strTotal</FONT></STRONG>\n");
	print ("			</td>\n");

	print ("		</tr>\n");

	### Display submit button and end of page
	print <<EndHTML;

		<tr>
			<td align=center colspan="7"><input id='hiddenButton' type="submit" value="Main Menu"></td>
		</tr>
	</table>
	</form>
	<br>
	
	$strLOGOUT_LINK

	</body>
</html>

EndHTML
	exit;
}else{
	#Display a login error
	printEmpLoginErr;
}

exit;


######################################################################
### Generate HTML for one week of code/hours from the current data.
### INPUT:
### - Week: number 1-2 indicating the week of the pay period to display
### USES:
### - @aStartDate - global variable containing the start date of the
###		period to display data for
### - $strUserId - global variable containing the user to display
###		data for
### - %TotalHrs - hash of totals for each hourly paycode type
### - %TotalDlrs - hash of totals for each dollarly paycode type
### OUTPUT:
### - prints out the generated HTML code
######################################################################
sub printWeekHours {
	# Get parameters
	my($nWeek) = @_;

	# Determine the date offset based on which week is being displayed
	# - first week - offset = 0 days
	# - second week - offset = 7 days
	my($nOffset) = 0;
	if ($nWeek == 2) {
		$nOffset = 7;
	}

	#Display a cell for each day of the week
	for($dailycount=0;$dailycount<7;$dailycount++){
		print ("			<td align=center>\n");

		# Display $nENTRIES_PER_DAY (3) code/hours control-pairs for each day
		for($dailyentrycount=0;$dailyentrycount<$nENTRIES_PER_DAY;$dailyentrycount++) {
			# Retrieve the code/hours from the database if they exist
			@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount+$nOffset);
			@aHours = getHours($strUserId, $dailyentrycount+1, @aCurrentDate);

			# if nothing found in db, set to default if applicable
			if (!(@aHours)) {
				if ($dailycount != 0 and $dailycount != 6 and $dailyentrycount == 0) {
					#if this entry is the first for a weekday then default to 7 regular hours
					$aHours[0] = "REG";
					$aHours[1] = "7";
				}else{
					#in all other cases, do not display
					$aHours[0] = "";
					$aHours[1] = "";
				}
			}

			# Do not display any values with 0 hours if they are:
			#	- second or third entry for a weekday
			#	- any entry for a weekend
			if (($aHours[1] == 0) and 
					(($dailycount != 0 and $dailycount != 6 and $dailyentrycount != 0) or
					 $dailycount == 0 or $dailycount == 6)) {
					$strCode = "";
					$strHours = "";
			}else{
				$strCode = $aHours[0];
				if ($aHours[1] == int($aHours[1])) {
					#Remove trailing decimals
					$strHours = sprintf("%d", $aHours[1]);
				}else{
					$strHours = sprintf("%.2f", $aHours[1]);
				}
			}

			#display code/hours
			print("<STRONG>$strCode &nbsp;$strHours</STRONG><BR>\n");

			#Calculate Totals
			if ($strCode ne "") {
				if ($strCode eq "MAT" or $strCode eq "MR" or $strCode eq "AP") {
					#Calculate totals for codes that are $ amounts
					if ($TotalDlrs{$strCode} == 0) {
						$TotalDlrs{$strCode} = $strHours;
					}else{
						$TotalDlrs{$strCode} += $strHours;
					}
				}else{
					#Calculate totals for codes that are hour amounts
					if ($TotalHrs{$strCode} == 0) {
						$TotalHrs{$strCode} = $strHours;
					}else{
						$TotalHrs{$strCode} += $strHours;
					}
				}
			}
		}
		print ("			</td>\n");

	}
}

exit;