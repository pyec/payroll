### ***********************************************************
### File: admin_sel_per.pl
### Purpose: To display a picklist of periods for the 
###		specified supervisor, and then submit that to adminview.pl
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;

#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');


# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n", $template_header->output;

$strLOGIN_PAGE = "/CorporateProjects/Timesheets/payadmin.html";
$strADM_LOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$mgr = $FORM{'mgr'};
$adminchoice = $FORM{'adminchoice'};

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	#Login successful, display the BU selection list and redirect accordingly
	
	# Get the payperiods for the specified supervisor
	$strChoices = getPeriods ("Old", $mgr);
	
#	printHeader;
	print <<EndHTML;
	<font color="#0000a0"><h1 align=center>HRM Payroll Administration</h1></font>
	<p><font color="#0000a0">$strEmpName, please select the historical period that you would like to view?</p>
	
	<form action="adminviewtemp.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="hidden" name="adminchoice" value="$adminchoice">
		<input type="hidden" name="mgr" value="$mgr">
		<P>Period: <select name="period">
			$strChoices
		</select></P>
		<input type="submit" value="Submit">
	</form>
	<br>
	
	<form action="adminlogintemp.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type="hidden" name="name" value="$name">
		<input type="hidden" name="pass" value="$pass">
		<input type="submit" ID="hiddenButton" value="Main Menu">	
	</form>

	$strADM_LOGOUT_LINK
	
</body>
</html>

EndHTML
print $template_footer->output;

	exit;

}else{
	#Display a login error
	printAdmLoginErr;
}


######################################################################
### Generate an <OPTION> list of pay periods
### INPUT:
### - Old: string indicating to retrieve the "Old" or future pay periods
### - MgrId: Userid of the manager to retrieve the pay periods for
### OUTPUT:
###	- strHTML: Contains the <OPTION> list of pay periods
######################################################################
sub getPeriods{
	# Get parameter indicating old or new periods
	my($strOld, $strMgrId) = @_;
	
	# Connect to the local db
	db_connect;
	
	### Retrieve the current pay period for the current user
	$strSQL = "SELECT CurrentPeriodId
				FROM Supervisors
				WHERE SupervisorId = '$strMgrId'";

	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	($strCurrentPeriodNum) = $sql_statement->fetchrow_array;

	# Get either the old or current periods, depending on the parameter
	if ($strOld eq "Old") {
		### Retrieve all Pay Periods previous to the current period
		$strSQL =  "SELECT PeriodId, Year 
					FROM Periods
					WHERE PeriodId < '$strCurrentPeriodNum'
					ORDER BY PeriodId DESC";
		$nMaxPeriods = 0;
	}else{
		### Retrieve the next $nPERIODS_TO_DISPLAY Pay Periods
		$strSQL =  "SELECT PeriodId, Year 
					FROM Periods
					WHERE PeriodId >= '$strCurrentPeriodNum'
					ORDER BY PeriodId";
		$nMaxPeriods = $nPERIODS_TO_DISPLAY;
	}
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
	my($strHTML) = "";
	my($nCountOfPeriods) = 0;
	while (my($strPeriodId, $strYear) = $sql_statement->fetchrow_array and 
		($nCountOfPeriods++ < $nMaxPeriods or $nMaxPeriods == 0)) {
		# Add an <OPTION> for each payperiod
		$strHTML .= "<OPTION value='$strPeriodId'>$strPeriodId</OPTION>\n";
	}

	## Disconnect from the local Access db
	$sql_statement->finish;
	$conn->disconnect;
	
	# return the HTML to display the Select list
	return $strHTML;
}
print $template_footer->output;


exit;
