### ***********************************************************
### File: adminview.pl
### Purpose: To display the employee hours for a specified 
###		division/supervisor in printable format.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
# Module for manipulating dates
use Date::Calc qw( Add_Delta_Days 
	                Month_to_Text);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;

#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');


# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n", $template_header->output;


# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name = $FORM{'name'};
$pass = $FORM{'pass'};
$mgr = $FORM{'mgr'};
$period = $FORM{'period'};
$adminchoice = $FORM{'adminchoice'};
$page = $FORM{'page'};
$strLOGIN_PAGE = "/CorporateProjects/Timesheets/payadmin.html";
$strADM_LOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";

# Default page to 1 if no value
if ($page == 0) {
	$page = 1;
}

#Define the next page values
$nNextPage = $page + 1;
$nPrevPage = $page - 1;

# Validate the login attempt
if ( validateAdmLogin($name, $pass) == 1) {
	# Check that a period parameter was passed
	if ($period eq "") {
		#if not, then default to current period for div selected
		# Connect to the local db
		db_connect;
		
		### Retrieve the current period
		$strSQL = "SELECT CurrentPeriodId, EmployeeName
					FROM qrySel_AllSupervisors
					WHERE SupervisorId = '$mgr'";
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
		($period, $strMgrName) = $sql_statement->fetchrow_array;
	
		## Disconnect from the local Access db
		$sql_statement->finish;
		$conn->disconnect;
	}else{
		#Retrieve the supervisor name
		# Connect to the local db
		db_connect;
		
		### Retrieve the current period
		$strSQL = "SELECT EmployeeName
					FROM Users
					WHERE UserId = '$mgr'";
		$sql_statement = $conn->prepare($strSQL) 
			or dienice("Can't prepare SQL statement:", $conn->errstr);
		$rsResult = $sql_statement->execute() 
			or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
	
		($strMgrName) = $sql_statement->fetchrow_array;
	
		## Disconnect from the local Access db
		$sql_statement->finish;
		$conn->disconnect;
	}

	# Get the period dates to display
	getPeriodDates($period);

	# Print a plain header - this page is to be printed so do not use the intranet template
	print <<EndHTML;
<HTML>
<HEAD>
<TITLE>HRM Attendance Tracking & Payroll Reimbursement Page</TITLE>
<STYLE MEDIA="PRINT">
#hiddenButton{visibility:hidden;}
#hiddenLink{visibility:hidden;}
</STYLE>

</HEAD>

<BODY>
	<table width="100%" border="0" cellspacing="2" cellpadding="2">
		<tr>
			<td width='33%' valign='top'>
				<font color="#0000a0"><STRONG>Time Sheet For: $strMgrName<BR>
				Pay Period : $period</STRONG><BR>
				<FONT size='-1'>(Printed By: $strEmpName)</FONT></font>
			</td>
	
			<td width='33%' valign='top'>
				<font color="#0000a0" size="-1">
				Start Date : $strStartDate<BR>
				End Date : $strEndDate<BR>
				Pay Day : $strPayDate</font>
			</td>

			<td width='34%' align='right' valign='bottom'><font color="#0000a0" size="-1">
				_________________________<BR>
				<STRONG>Supervisor Signature</STRONG>
			</font></td>
		</tr>
	</table>
EndHTML

	### Start table of hours
	print ("	<table width='100%' align=center border='0' cellspacing=3 cellpadding=3 frame='box'>\n");

	### Repeat header and data rows for each employee for the selected supervisor
	# Connect to the local db
	db_connect;
	
	### Retrieve the employees for the selected supervisor
	$strSQL = "SELECT UserId, EmployeeName, EmployeeNumber, StatusId
				FROM Users
				WHERE SupervisorId = '$mgr'
				ORDER BY EmployeeNumber";
	$sql_statement = $conn->prepare($strSQL) 
		or dienice("Can't prepare SQL statement:", $conn->errstr);
	$rsResult = $sql_statement->execute() 
		or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);

	#Use counter to only display $nUSERS_PER_PAGE (8) users per screen
	$nUserCount = 0; 

		
	while (($strCurrentUserId, $strCurrentEmpName, $strCurrentEmpNum, $strCurrentStatus) = $sql_statement->fetchrow_array and 
			$nUserCount++ <= ($nUSERS_PER_PAGE * $page)) {
		# Depending on the page, skip users that have been previously displayed
		if ($nUserCount <= ($nUSERS_PER_PAGE * $page) and $nUserCount > ($nUSERS_PER_PAGE * ($page-1))) {

			# Display column headers with weekdays and dates for first and second week
			print ("		<tr>\n");
			print ("			<td align='center' width='7%'><font size='-3'><STRONG>Name</STRONG></td>\n");
			for($dailycount=0;$dailycount<14;$dailycount++){
				#calculate the date to display
				@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount);
				$strCurrentDate = sprintf("%3.3s %s", Month_to_Text($aCurrentDate[1]), $aCurrentDate[2]);
		
				#print the html
				print ("			<td align='center' width='5%'><font size='-3'><STRONG>$strSHORT_WEEKDAYS[($dailycount%7)] <br>$strCurrentDate</STRONG></font></td>\n");
			}
			print ("			<td align='center' width='11%'><font size='-3'><b>Totals</td>\n");
			print ("		</tr>\n");
	
			# Display daily data for user
			print ("		<tr>\n");
			print ("			<td width='8%'><font size='-3'>$strCurrentEmpName<BR>$strCurrentEmpNum</td>\n");
			printHours ($strCurrentUserId, $strCurrentStatus);
			print ("			<td align='left' width='8%'><font size='-3'>\n");
	
			# Display the totals for the user
			$strTotal = 0;
			foreach $keys (keys %TotalHrs) {
				print ("				$keys: $TotalHrs{$keys}<BR>\n");
				$strTotal += $TotalHrs{$keys};
			}
			print ("				<FONT color='#0000a0'>*Total: $strTotal</FONT>\n");
		
			# Display total dollars
			$strTotal = 0;
			foreach $keys (keys %TotalDlrs) {
				$strDollars = sprintf("\$%.2f", $TotalDlrs{$keys});
				print ("				<BR>$keys: $strDollars\n");
				$strTotal += $TotalDlrs{$keys};
			}
			if ($strTotal > 0) {
				$strTotal = sprintf("\$%.2f", $strTotal);
				print ("				<BR><FONT color='#0000a0'>*Total: $strTotal</FONT>\n");
			}
			print ("			</td>\n");
			print ("		</tr>\n");
			
			#Clear the total values
			%TotalHrs = ();
			%TotalDlrs = ();
			
			# Do not display the 'Next Page' control unless a next page is required
			$strNextPageControl = "";
			
		}elsif ($nUserCount == (($nUSERS_PER_PAGE * $page) + 1)) {
			# A next page is required - display the next page control
			$strNextPageControl = "<input type='submit' ID='hiddenButton' value='Next Page &gt;&gt;'><BR><BR>";
		}
	}
	
	#Display a previous page button if this is not the first page
	if ($page > 1) {
		$strPrevPageControl = "<input type='submit' ID='hiddenButton' value='&lt;&lt; Prev Page'>&nbsp;";
	}
	
	print <<EndHTML;
	</TABLE>
	<P align=center>
		<form action="adminviewtemp.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
			<input type="hidden" name="mgr" value="$mgr">
			<input type="hidden" name="period" value="$period">
			<input type="hidden" name="page" value="$nPrevPage">
			$strPrevPageControl
		</form>

		<form action="adminviewtemp.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
			<input type="hidden" name="mgr" value="$mgr">
			<input type="hidden" name="period" value="$period">
			<input type="hidden" name="page" value="$nNextPage">
			$strNextPageControl
		</form>

		<form action="adminlogintemp.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type="hidden" name="name" value="$name">
			<input type="hidden" name="pass" value="$pass">
			<input type="submit" ID="hiddenButton" value="Main Menu">	
		</form>
	</P>

	$strADM_LOGOUT_LINK

</BODY>
</HTML>
EndHTML
 

}else{
	#Display a login error
	printAdmLoginErr;
}

print $template_footer->output;
exit;



######################################################################
### Generate HTML for 2 weeks of code/hours from the current data.
### INPUT:
### - User: Userid for the data to display
### - User_Status: Status of the user to display - Part-time or Full-time
### USES:
### - @aStartDate - global variable containing the start date of the
###		period to display data for
### - %TotalHrs - hash of totals for each hourly paycode type
### - %TotalDlrs - hash of totals for each dollarly paycode type
### OUTPUT:
### - prints out the generated HTML code
######################################################################
sub printHours {
	# Get parameters
	my($strUser, $strStatus) = @_;

	#Display a cell for each day of the week
	for($dailycount=0;$dailycount<14;$dailycount++){
		print ("			<td align=center><font size='-2'>\n");

		# Display $nENTRIES_PER_DAY (3) code/hours control-pairs for each day
		for($dailyentrycount=0;$dailyentrycount<$nENTRIES_PER_DAY;$dailyentrycount++) {
			# Retrieve the code/hours from the database if they exist
			@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount+$nOffset);
			@aHours = getHours($strUser, $dailyentrycount+1, @aCurrentDate);

			# if nothing found in db, set to default if applicable
			if (!(@aHours)) {
				if (($dailycount%7) != 0 and ($dailycount%7) != 6 and $dailyentrycount == 0) {
					#if this entry is the first for a weekday then default to 7 regular hours
					$aHours[0] = "REG";
					$aHours[1] = "7";
				}else{
					#in all other cases, do not display
					$aHours[0] = "";
					$aHours[1] = "";
				}
			}

			# Do not display any values with 0 hours if they are:
			#	- second or third entry for a weekday
			#	- any entry for a weekend
			if (($aHours[1] == 0) and 
					(($dailycount != 0 and $dailycount != 6 and $dailyentrycount != 0) or
					 $dailycount == 0 or $dailycount == 6)) {
				$strCode = "";
				$strHours = "";
			}else{
				$strCode = $aHours[0];
				if ($aHours[1] == int($aHours[1])) {
					#Remove trailing decimals
					$strHours = sprintf("%d", $aHours[1]);
				}else{
					$strHours = sprintf("%.2f", $aHours[1]);
				}
			}

			#Display code/hours if not default and not blank, or
			#	if default, but user is part time
			if ((($strCode ne "REG" or $strHours != 7) and $strCode ne "") or
				($strCode eq "REG" and $strHours == 7 and $strStatus eq "P")){
				print("$strCode &nbsp;$strHours<BR>\n");
			}else{
				print("&nbsp;<BR>\n");
			}

			#Calculate Totals
			if ($strCode ne "") {
				if ($strCode eq "MAT" or $strCode eq "MR" or $strCode eq "AP") {
					#Calculate totals for codes that are $ amounts
					if ($TotalDlrs{$strCode} == 0) {
						$TotalDlrs{$strCode} = $strHours;
					}else{
						$TotalDlrs{$strCode} += $strHours;
					}
				}else{
					#Calculate totals for codes that are hour amounts
					if ($TotalHrs{$strCode} == 0) {
						$TotalHrs{$strCode} = $strHours;
					}else{
						$TotalHrs{$strCode} += $strHours;
					}
				}
			}
		}
		print ("			</font></td>\n");

	}
}
 

exit;