### ***********************************************************
### File: empinput.pl
### Purpose: To display the period hours in edit mode.
### ***********************************************************

### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
# Module for manipulating dates
use Date::Calc qw( Add_Delta_Days 
	                Month_to_Text 
	                English_Ordinal);
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;
# Perl module to use HTML Templates from CGI scripts
use HTML::Template;

# set the current working directory
setHomeDir();

#print "Content-type:text/html\n\n";

#adding header to the page
my $template_header = HTML::Template->new(filename => 'header.tmpl');
#adding footer to the page
my $template_footer = HTML::Template->new(filename => 'footer.tmpl');
$strLOGIN_PAGE = "/corporateprojects/timesheets/";
$strLOGOUT_LINK = "<div align='right'><a id='hiddenLink' href='$strLOGIN_PAGE'>Logout</a></div>";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name=$FORM{'name'};
$pass=$FORM{'pass'};
$period=$FORM{'period'};

# Validate the login attempt
if ( validateEmpLogin($name, $pass) == 1) {
	#Login successful, display the input hours page

	# Get the pay codes
	getCodes($strEmpDivId);

	# Get period start, end and pay dates in string form
	getPeriodDates($period);

	#printHeader;
	# send the obligatory Content-Type and print the template output
	print "Content-Type: text/html\n\n", $template_header->output;
	print <<EndHTML;

	<h1 align="center">Attendance Tracking & Payroll Reimbursement Page</h1>

	<P align="center"><FONT color="#0000a0">Welcome $strEmpName.</FONT></P>
	
	<P>
<table>
<tr>
<td><STRONG>Pay Period:&nbsp;&nbsp;&nbsp;</STRONG></td>
<td>$period</td>
</tr>
<tr>
<td><STRONG>Start Date: </STRONG></td>
<td>$strStartDate</td>
</tr>
<tr>
<td><STRONG>End Date: </STRONG></td>
<td> $strEndDate</td>
</tr>
<tr>
<td><STRONG>Pay Day: </STRONG></td>
<td>$strPayDate</td>
</tr>
<tr><td height=20>&nbsp;</td><td>&nbsp;</td></tr>
</table>

	
	<INPUT type='button' value='Pay Code Chart' onClick="window.open('paycodes.pl?div=$strEmpDivId','mywindow','width=450,height=200,scrollbars=yes,resizable=yes')">

<br /><br /><br />

	<FORM name="chart" action="posthourstemp.pl" method="post">
		<input type="hidden" name="userid" value="$strUserId">
		<input type=hidden name="name" value="$name">
		<input type=hidden name="pass" value="$pass">
		<input type=hidden name="period" value="$period">

EndHTML

	# Connect to the local db once to reduce overhead and improve performance
	db_connect;
	
	### Start table of hours
	print ("	<table width='100%' cellspacing=5 cellpadding=5 border=0>\n");

	### Display column headers with weekdays and dates for first week
	print ("		<tr>\n");
	for($dailycount=0;$dailycount<7;$dailycount++){
		#calculate the date to display
		@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount);
		$strCurrentDate = sprintf("%s&nbsp;%s", Month_to_Text($aCurrentDate[1]), English_Ordinal($aCurrentDate[2]));

		#print the html
		print ("			<td align=left><STRONG>$strWEEKDAYS[$dailycount]</STRONG> <br>$strCurrentDate</td>\n");
	}
	print ("		</tr>\n");

	### Display first week of codes and hours
	print ("		<tr>\n");
	# Display the cells for week #1
	printWeekHours (1);
	print ("		</tr>\n");


	### Display column headers with weekdays and dates for second week (first week + 7)
	print ("		<tr>\n");
	for($dailycount=0;$dailycount<7;$dailycount++){
		#calculate the date to display
		@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount + 7);
		$strCurrentDate = sprintf("%s&nbsp;%s", Month_to_Text($aCurrentDate[1]), English_Ordinal($aCurrentDate[2]));

		#print the html
		print ("			<td align=left><STRONG>$strWEEKDAYS[$dailycount]</STRONG> <br>$strCurrentDate</td>\n");
	}
	print ("		</tr>\n");

	### Display second week of codes and hours
	print ("		<tr>\n");
	# Display the cells for week #2
	printWeekHours (2);
	print ("		</tr>\n");

	## Disconnect from the local Access db after repeated calls to getHours()
	#$sql_statement->finish;
	$conn->disconnect;

	### Display submit button and end of page
	print <<EndHTML;

		<tr>
			<td align=center colspan="7"><input type="submit" value="Submit">&nbsp;&nbsp;<input type="Reset"></td>
		</tr>
	</table>
	</form>
	<br>
	
	<FORM action='usertemp.pl' method='post' name='menu'>
		<input type='hidden' name='userid' value='$strUserId'>
		<input type='hidden' name='name' value='$name'>
		<input type='hidden' name='pass' value='$pass'>
		<P align='center'><input type="submit" ID="hiddenButton" value="Main Menu"></P>
	</FORM>
	$strLOGOUT_LINK

	</body>
</html>

EndHTML
print $template_footer->output;
	exit;
}else{
	#Display a login error
	printEmpLoginErr;
}
print $template_footer->output;
exit;


######################################################################
### Generate an <OPTION> list of pay codes with the specified code
### selected as the active entry
### INPUT:
### - Code: Pay code to be the active entry
### OUTPUT:
###	- $options: a string containing the <OPTION> list
######################################################################
sub getOptions{
	# Get parameters
	my($strCode) = @_;

	#Init
	my($strOptions) = "";
	my($strSelected) = "";

	# strings used to construct the <OPTION> tags
	$strBegin = "					<OPTION";
	$strValue = " value='";
	$strMiddle = "'>";
	$strEnd = "</OPTION>\n";

	# use the @aPayCodeId array to generate the list
	foreach $strPayCode (@aPayCodeId) {
		if ($strCode eq $strPayCode) {
			$strSelected = " SELECTED";
		}else{
			$strSelected = "";
		}
		$strOptions .= $strBegin . $strSelected . $strValue . 
						$strPayCode . $strMiddle . $strPayCode . $strEnd;
	}
	
	return $strOptions;
}



######################################################################
### Generate HTML for one week of code/hours from the current data.
### INPUT:
### - Week: number 1-2 indicating the week of the pay period to display
### USES:
### - @aStartDate - global variable containing the start date of the
###		period to display data for
### - $strUserId - global variable containing the user to display
###		data for
### OUTPUT:
### - prints out the generated HTML code
######################################################################
sub printWeekHours {
	# Get parameters
	my($nWeek) = @_;

	# Define the header titles for each daily cell in the table
	# my($strCellHdr) = "Code&nbsp;/&nbsp;#&nbsp;Hrs.<BR>";
	my($strCellHdr) = "";

	# Determine the date offset based on which week is being displayed
	# - first week - offset = 0 days
	# - second week - offset = 7 days
	my($nOffset) = 0;
	if ($nWeek == 2) {
		$nOffset = 7;
	}

	#Display a cell for each day of the week
	for($dailycount=0;$dailycount<7;$dailycount++){
		print ("			<td>$strCellHdr \n");

		# Display $nENTRIES_PER_DAY (3) code/hours control-pairs for each day
		for($dailyentrycount=0;$dailyentrycount<$nENTRIES_PER_DAY;$dailyentrycount++) {
			# Retrieve the code/hours from the database if they exist
			@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount+$nOffset);
			@aHours = getHours($strUserId, $dailyentrycount+1, @aCurrentDate);

			# if nothing found in db, set to default if applicable
			if (!(@aHours)) {
				if ($dailycount != 0 and $dailycount != 6 and $dailyentrycount == 0) {
					#if this entry is the first for a weekday then default to 7 regular hours
					$aHours[0] = "REG";
					$aHours[1] = 7;
				}else{
					#in all other cases default to 0 regular hours
					$aHours[0] = "REG";
					$aHours[1] = 0;
				}
			}
				
			#calculate the values to populate in the codes and hours controls
			#NOTE: SELECT name is of the format 'dayXcodeY' where X is 1-14 and Y is 1-3
			#NOTE: INPUT name is of the format 'dayXhrsY' where X is 1-14 and Y is 1-3
			$strSelectName = 'day' . ($dailycount+1+$nOffset) . 'code' . ($dailyentrycount+1);
			$strInputName = 'day' . ($dailycount+1+$nOffset) . 'hrs' . ($dailyentrycount+1);
			if ($aHours[1] == int($aHours[1])) {
				#Remove trailing decimals
				$strInputValue = sprintf("%d", $aHours[1]);
			}else{
				$strInputValue = sprintf("%.2f", $aHours[1]);
			}
			$strOptions = getOptions($aHours[0]);

			#display one pair of entry controls
			print("				<select name='$strSelectName'>\n $strOptions\n				</SELECT> ");
			print("<input type='text' name='$strInputName' value='$strInputValue' maxlength='5' size=3><br>\n");
		}
		print ("			</td>\n");

	}
}
print $template_footer->output;
exit;