### ***********************************************************
### File: posthours.pl
### Purpose: To save the period hours to the database
### ***********************************************************

### ***********************************************************
### CHANGE MANAGEMENT CONTROL
### ***********************************************************
### Aug 3rd, 2011 (WO# 6671 | RFC# 00000)
### Daya and Gian commented out line ~74-76 starting with
### if ($strCurrentCode eq 'MAT') {
### effectively removing the hard-coding of $10 for MAT code
### ***********************************************************


### System modules:
# Module for dienice() errorhandler
use hrm_errorhandler;
# Module for parsing form input
use hrm_parseinput(getFormValues);
# Module for manipulating dates
use Date::Calc qw( Add_Delta_Days );
#module for setting the current working directory
use hrm_homedir;

### Local modules:
# Module for local TimeSheet.mdb database access
# - $conn is the database connection
use hrm_db_conn_timesheets;
# Module for common TimeSheet application utilities
use hrm_timesheet_util;

# set the current working directory
setHomeDir();

print "Content-type:text/html\n\n";

# Get the values submitted by the form
%FORM = getFormValues();
$strUserId = $FORM{'userid'};
$name=$FORM{'name'};
$pass=$FORM{'pass'};
$period=$FORM{'period'};

# Validate the login attempt
if ( validateEmpLogin($name, $pass) == 1) {
	#Login successful, save the data and redirect to the display page

	#print the page indicating that the data is being saved
	printHeader;
	print <<EndHTML;
		<FORM name="chart" id="chart" action="displayhours.pl" method="post">
			<input type="hidden" name="userid" value="$strUserId">
			<input type=hidden name="name" value="$name">
			<input type=hidden name="pass" value="$pass">
			<input type=hidden name="period" value="$period">
		</FORM>
		<P align=center><STRONG>Saving data
EndHTML
	
	# Retrieve the existing data for the current user and the specified period
	getPeriodDates ($period);

	# Connect to the local db
	db_connect;
	
	#Turn off buffering so the HTML page will be updated immediately
	$|=1;

	# Process data for each day in the period
	for($dailycount=0;$dailycount<14;$dailycount++){
		#get weekday number, zero based -> 0-6 (= $dailycount mod 7)
		$nWeekDay = $dailycount%7;

		#Status bar
		print (".");

		# Process $nENTRIES_PER_DAY (3) for each day
		for($dailyentrycount=0;$dailyentrycount<$nENTRIES_PER_DAY;$dailyentrycount++) {
			# Retrieve the code/hours from the form submission
			$strCurrentCode = $FORM{'day' . ($dailycount+1) . 'code' . ($dailyentrycount+1)};
			$nCurrentHours = $FORM{'day' . ($dailycount+1) . 'hrs' . ($dailyentrycount+1)};
			
			### ******************************************************* ###
			# If Code = 'MAT', then hours must be set to $10 - HARDCODING #
			### ******************************************************* ###
			#if ($strCurrentCode eq 'MAT') {
			#	$nCurrentHours = 10;
			#}
			
			# Retrieve the code/hours from the database if they exist
			@aCurrentDate = Add_Delta_Days(@aStartDate,$dailycount+$nOffset);
			@aHours = getHours($strUserId, $dailyentrycount+1, @aCurrentDate);
			$strCurrentDate = "$aCurrentDate[0]-$aCurrentDate[1]-$aCurrentDate[2]";

			if (!(@aHours)) {
				if ($strCurrentCode eq "REG" and 
					($nCurrentHours == 7 and ($nWeekDay != 0 and $nWeekDay != 6 and $dailyentrycount == 0)) or
					($nCurrentHours == 0 and ($nWeekDay == 0 or $nWeekDay == 6)) or 
					($nCurrentHours == 0 and ($nWeekDay != 0 and $nWeekDay != 6 and $dailyentrycount != 0))) {
					# Do not store the following in the database:
					# 1) Default - REG 7, Mon - Fri for the first daily entry
					# 2) Default - REG 0, Sat - Sun
					# 3) REG 0, Mon - Fri for the second and third daily entry
					#		(but not the first daily entry -> it should be saved)
					$strSQL = "";
				}else{
					# if nothing found in db, then add it if it is not the default data
					$strSQL = "INSERT INTO User_Hours ( UserId, HoursDate, EntryIndex, PayCodeId, Hours )
								SELECT '$strUserId', 
										$strHASH$strCurrentDate$strHASH, 
										$dailyentrycount+1, 
										'$strCurrentCode', 
										$nCurrentHours";
				}
			}else{
				if ($aHours[0] eq "REG" and 
					($aHours[1] == 7 and ($nWeekDay != 0 and $nWeekDay != 6 and $dailyentrycount == 0)) or
					($aHours[1] == 0 and ($nWeekDay == 0 or $nWeekDay == 6)) or 
					($aHours[1] == 0 and ($nWeekDay != 0 and $nWeekDay != 6 and $dailyentrycount != 0))) {
					# Delete any default records - same rules as above - 1), 2) and 3)
					$strSQL = "DELETE FROM User_Hours
								WHERE UserId = '$strUserId' AND 
									HoursDate = $strHASH$strCurrentDate$strHASH AND
									EntryIndex = ($dailyentrycount+1)";
				}else{
					# if data already exists then update it
					$strSQL = "UPDATE User_Hours
								SET PayCodeId = '$strCurrentCode',
									Hours = $nCurrentHours
								WHERE UserId = '$strUserId' AND 
									HoursDate = $strHASH$strCurrentDate$strHASH AND
									EntryIndex = ($dailyentrycount+1)";
				}
			}

			#Save the data to the database
			if ($strSQL ne "") {
				$sql_statement = $conn->prepare($strSQL)
					or dienice("Can't prepare SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
				
				$rsResult = $sql_statement->execute()
					or dienice("Can't execute SQL statement:<BR>\n $strSQL <BR>\n", $conn->errstr);
			}
		}
	}

	## Disconnect from the local db
	#$sql_statement->finish;
	$conn->disconnect;

	#End the progress page
	print ("</P></BODY></HTML>");
	
	#Turn buffering back on to continue processing
	$|=0;

	# redirect to the display page
	print ("<script language=JavaScript>document.chart.submit();</script>");

	exit;
}else{
	#Display a login error
	printEmpLoginErr;
}

exit;
